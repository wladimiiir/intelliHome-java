package sk.wladimiiir.intellihome.common.utils;

import java.util.concurrent.Callable;

/**
 * @author wladimiiir
 * @since 7/5/15
 */
public class ExceptionUtils {
    private ExceptionUtils() {
    }

    public static <T> T runtimeException(Callable<T> catchCall) {
        try {
            return catchCall.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
