package sk.wladimiiir.intellihome.common.utils;

import java.time.Duration;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author wladimiiir
 * @since 7/5/15
 */
public class TimerUtils {
    private TimerUtils() {
    }

    public static Timer startTimeout(Runnable timeoutRunnable, Duration timeout) {
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                timeoutRunnable.run();
            }
        }, timeout.toMillis());
        return timer;
    }

    public static Timer startInterval(Runnable intervalRunnable, long interval) {
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                intervalRunnable.run();
            }
        }, 0, interval);
        return timer;
    }
}
