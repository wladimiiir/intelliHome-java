package sk.wladimiiir.intellihome.device.bbb;

import sk.wladimiiir.intellihome.device.*;
import sk.wladimiiir.intellihome.device.exception.DigitalDeviceError;
import sk.wladimiiir.intellihome.device.exception.OneWireDeviceError;
import sk.wladimiiir.intellihome.device.exception.PinException;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author wladimiiir
 * @since 7/4/15
 */
@Named("beagleBone")
@Singleton
public class BeagleBoneDeviceManager implements DeviceManager {
    private static final Path ONE_WIRE_DEVICES_PATH = Paths.get("/sys/devices/w1_bus_master1");

    private final List<Device> openedDevices = new ArrayList<>();

    @Inject
    private volatile BeagleBonePinMapper pinMapper;

    @Override
    public OneWireDevice getOneWireDevice(String serialNumber) throws OneWireDeviceError {
        final Path devicePath = ONE_WIRE_DEVICES_PATH.resolve(serialNumber);

        if (Files.exists(devicePath)) {
            return new BeagleBoneOneWireDevice(devicePath, serialNumber);
        } else {
            throw new OneWireDeviceError("Cannot find device on path: " + devicePath);
        }
    }

    @Override
    public DigitalInputPinDevice getDigitalInputPinDevice(Pin pin) throws DigitalDeviceError {
        final BeagleBoneDigitalPinDevice device = new BeagleBoneDigitalPinDevice(pin, BeagleBoneDigitalPinDevice.Direction.INPUT);
        device.open();
        openedDevices.add(device);
        return device;
    }

    @Override
    public DigitalOutputPinDevice getDigitalOutputPinDevice(Pin pin) throws DigitalDeviceError {
        final BeagleBoneDigitalPinDevice device = new BeagleBoneDigitalPinDevice(pin, BeagleBoneDigitalPinDevice.Direction.OUTPUT);
        device.open();
        openedDevices.add(device);
        return device;
    }

    @Override
    public Pin getPinByKey(String key) throws PinException {
        final Optional<Pin> pin = pinMapper.getPinByKey(key);
        if (!pin.isPresent()) {
            throw new PinException("Pin not found by given key: " + key);
        }
        return pin.get();
    }

    @Override
    public Pin getPinByName(String name) throws PinException {
        final Optional<Pin> pin = pinMapper.getPinByName(name);
        if (!pin.isPresent()) {
            throw new PinException("Pin not found by given name: " + name);
        }
        return pin.get();
    }

    @Override
    public Pin getPinByGPIO(int gpio) throws PinException {
        final Optional<Pin> pin = pinMapper.getPinByGPIO(gpio);
        if (!pin.isPresent()) {
            throw new PinException("Pin not found by given GPIO number: " + gpio);
        }
        return pin.get();
    }

    @Override
    public void close() {
        openedDevices.forEach(device -> {
            try {
                device.close();
            } catch (DigitalDeviceError error) {
                error.printStackTrace();
            }
        });
    }
}
