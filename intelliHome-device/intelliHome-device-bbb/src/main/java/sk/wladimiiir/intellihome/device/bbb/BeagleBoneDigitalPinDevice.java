package sk.wladimiiir.intellihome.device.bbb;

import sk.wladimiiir.intellihome.device.DigitalInputPinDevice;
import sk.wladimiiir.intellihome.device.DigitalOutputPinDevice;
import sk.wladimiiir.intellihome.device.Pin;
import sk.wladimiiir.intellihome.device.exception.DigitalDeviceError;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author wladimiiir
 * @since 7/4/15
 */
public class BeagleBoneDigitalPinDevice implements DigitalInputPinDevice, DigitalOutputPinDevice {
    private static final Path GPIO_DEVICES_PATH = Paths.get("/sys/class/gpio/");
    private static final Path EXPORT_PATH = GPIO_DEVICES_PATH.resolve("export");
    private static final Path UNEXPORT_PATH = GPIO_DEVICES_PATH.resolve("unexport");

    private final Pin pin;
    private final Direction direction;

    public BeagleBoneDigitalPinDevice(Pin pin, Direction direction) {
        this.pin = pin;
        this.direction = direction;
    }

    public void open() throws DigitalDeviceError {
        try {
            //export the pin so it becomes available
            Files.write(EXPORT_PATH, String.valueOf(pin.getNumber()).getBytes());
            //set the direction of the pin
            Files.write(directionPath(), direction.getValue().getBytes());
        } catch (IOException e) {
            if (e.getMessage().contains("busy")) {
                //GPIO already exported
                return;
            }
            throw new DigitalDeviceError("Cannot export GPIO on pin: " + pin.toString(), e);
        }
    }

    public void close() throws DigitalDeviceError {
        try {
            //unexport pin
            Files.write(UNEXPORT_PATH, String.valueOf(pin.getNumber()).getBytes());
        } catch (IOException e) {
            throw new DigitalDeviceError("Cannot unexport GPIO on pin: " + pin.toString());
        }
    }

    private Path devicePath() {
        return GPIO_DEVICES_PATH.resolve("gpio" + pin.getNumber());
    }

    private Path directionPath() {
        return devicePath().resolve("direction");
    }

    private Path valuePath() {
        return devicePath().resolve("value");
    }

    @Override
    public int readValue() throws DigitalDeviceError {
        try {
            return Files.readAllBytes(valuePath())[0];
        } catch (IOException e) {
            throw new DigitalDeviceError("Cannot read value from pin: " + pin.toString(), e);
        }
    }

    @Override
    public Pin getPin() {
        return pin;
    }

    @Override
    public void low() throws DigitalDeviceError {
        writeValue(LOW);
    }

    @Override
    public void high() throws DigitalDeviceError {
        writeValue(HIGH);
    }

    @Override
    public void writeValue(int value) throws DigitalDeviceError {
        try {
            Files.write(valuePath(), String.valueOf(value).getBytes());
        } catch (IOException e) {
            throw new DigitalDeviceError("Cannot write value to pin: " + pin.toString(), e);
        }
    }

    public enum Direction {
        INPUT("in"),
        OUTPUT("out");

        private final String value;

        Direction(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
