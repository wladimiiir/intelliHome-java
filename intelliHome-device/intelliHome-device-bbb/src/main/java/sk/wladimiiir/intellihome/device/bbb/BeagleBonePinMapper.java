package sk.wladimiiir.intellihome.device.bbb;

import sk.wladimiiir.intellihome.device.Pin;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * https://insigntech.files.wordpress.com/2013/09/bbb_pinouts.jpg
 *
 * @author wladimiiir
 * @since 7/4/15
 */
@Named
@Singleton
public class BeagleBonePinMapper {
    private final static List<PinMapping> MAPPING = Arrays.asList(
            new PinMapping("USR0", "USR0", 53),
            new PinMapping("USR1", "USR1", 54),
            new PinMapping("USR2", "USR2", 55),
            new PinMapping("USR3", "USR3", 56),
            new PinMapping("GPIO1_6", "P8_3", 38),
            new PinMapping("GPIO1_7", "P8_4", 39),
            new PinMapping("GPIO1_2", "P8_5", 34),
            new PinMapping("GPIO1_3", "P8_6", 35),
            new PinMapping("TIMER4", "P8_7", 66),
            new PinMapping("TIMER7", "P8_8", 67),
            new PinMapping("TIMER5", "P8_9", 69),
            new PinMapping("TIMER6", "P8_10", 68),
            new PinMapping("GPIO1_13", "P8_11", 45),
            new PinMapping("GPIO1_12", "P8_12", 44),
            new PinMapping("EHRPWM2B", "P8_13", 23),
            new PinMapping("GPIO0_26", "P8_14", 26),
            new PinMapping("GPIO1_15", "P8_15", 47),
            new PinMapping("GPIO1_14", "P8_16", 46),
            new PinMapping("GPIO0_27", "P8_17", 27),
            new PinMapping("GPIO2_1", "P8_18", 65),
            new PinMapping("EHRPWM2A", "P8_19", 22),
            new PinMapping("GPIO1_31", "P8_20", 63),
            new PinMapping("GPIO1_30", "P8_21", 62),
            new PinMapping("GPIO1_5", "P8_22", 37),
            new PinMapping("GPIO1_4", "P8_23", 36),
            new PinMapping("GPIO1_1", "P8_24", 33),
            new PinMapping("GPIO1_0", "P8_25", 32),
            new PinMapping("GPIO1_29", "P8_26", 61),
            new PinMapping("GPIO2_22", "P8_27", 86),
            new PinMapping("GPIO2_24", "P8_28", 88),
            new PinMapping("GPIO2_23", "P8_29", 87),
            new PinMapping("GPIO2_25", "P8_30", 89),
            new PinMapping("UART5_CTSN", "P8_31", 10),
            new PinMapping("UART5_RTSN", "P8_32", 11),
            new PinMapping("UART4_RTSN", "P8_33", 9),
            new PinMapping("UART3_RTSN", "P8_34", 81),
            new PinMapping("UART4_CTSN", "P8_35", 8),
            new PinMapping("UART3_CTSN", "P8_36", 80),
            new PinMapping("UART5_TXD", "P8_37", 78),
            new PinMapping("UART5_RXD", "P8_38", 79),
            new PinMapping("GPIO2_12", "P8_39", 76),
            new PinMapping("GPIO2_13", "P8_40", 77),
            new PinMapping("GPIO2_10", "P8_41", 74),
            new PinMapping("GPIO2_11", "P8_42", 75),
            new PinMapping("GPIO2_8", "P8_43", 72),
            new PinMapping("GPIO2_9", "P8_44", 73),
            new PinMapping("GPIO2_6", "P8_45", 70),
            new PinMapping("GPIO2_7", "P8_46", 71),
            new PinMapping("UART4_RXD", "P9_11", 30),
            new PinMapping("GPIO1_28", "P9_12", 60),
            new PinMapping("UART4_TXD", "P9_13", 31),
            new PinMapping("EHRPWM1A", "P9_14", 50),
            new PinMapping("GPIO1_16", "P9_15", 48),
            new PinMapping("EHRPWM1B", "P9_16", 51),
            new PinMapping("I2C1_SCL", "P9_17", 5),
            new PinMapping("I2C1_SDA", "P9_18", 4),
            new PinMapping("I2C2_SCL", "P9_19", 13),
            new PinMapping("I2C2_SDA", "P9_20", 12),
            new PinMapping("UART2_TXD", "P9_21", 3),
            new PinMapping("UART2_RXD", "P9_22", 2),
            new PinMapping("GPIO1_17", "P9_23", 49),
            new PinMapping("UART1_TXD", "P9_24", 15),
            new PinMapping("GPIO3_21", "P9_25", 117),
            new PinMapping("UART1_RXD", "P9_26", 14),
            new PinMapping("GPIO3_19", "P9_27", 115),
            new PinMapping("SPI1_CS0", "P9_28", 113),
            new PinMapping("SPI1_D0", "P9_29", 111),
            new PinMapping("SPI1_D1", "P9_30", 112),
            new PinMapping("SPI1_SCLK", "P9_31", 110),
            new PinMapping("CLKOUT2", "P9_41", 20),
            new PinMapping("GPIO0_7", "P9_42", 7)
    );

    public Optional<Pin> getPinByKey(String key) {
        final Optional<PinMapping> mapping = MAPPING.parallelStream()
                .filter(pinMapping -> pinMapping.key.equals(key))
                .findFirst();
        if (mapping.isPresent()) {
            return Optional.of(new Pin(mapping.get().key, mapping.get().gpio));
        } else {
            return Optional.empty();
        }
    }

    public Optional<Pin> getPinByName(String name) {
        final Optional<PinMapping> mapping = MAPPING.parallelStream()
                .filter(pinMapping -> pinMapping.name.equals(name))
                .findFirst();
        if (mapping.isPresent()) {
            return Optional.of(new Pin(mapping.get().key, mapping.get().gpio));
        } else {
            return Optional.empty();
        }
    }

    public Optional<Pin> getPinByGPIO(int gpio) {
        final Optional<PinMapping> mapping = MAPPING.parallelStream()
                .filter(pinMapping -> pinMapping.gpio == gpio)
                .findFirst();
        if (mapping.isPresent()) {
            return Optional.of(new Pin(mapping.get().key, mapping.get().gpio));
        } else {
            return Optional.empty();
        }
    }

    private static class PinMapping {
        private final String name;
        private final String key;
        private final int gpio;

        public PinMapping(String name, String key, int gpio) {
            this.name = name;
            this.key = key;
            this.gpio = gpio;
        }
    }
}
