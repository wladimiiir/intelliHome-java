package sk.wladimiiir.intellihome.device.bbb;

import sk.wladimiiir.intellihome.device.OneWireDevice;
import sk.wladimiiir.intellihome.device.exception.OneWireDeviceError;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @author wladimiiir
 * @since 7/4/15
 */
public class BeagleBoneOneWireDevice implements OneWireDevice {
    private static final String W1_SLAVE = "w1_slave";

    private final Path devicePath;
    private final String serialNumber;

    public BeagleBoneOneWireDevice(Path devicePath, String serialNumber) {
        this.devicePath = devicePath;
        this.serialNumber = serialNumber;
    }

    @Override
    public String getSerialNumber() {
        return serialNumber;
    }

    @Override
    public String readValue() throws OneWireDeviceError {
        final Path valuePath = devicePath.resolve(W1_SLAVE);

        if (!Files.exists(devicePath) || !Files.exists(valuePath)) {
            throw new OneWireDeviceError("Device not found on path: " + devicePath);
        }

        try {
            return new String(Files.readAllBytes(valuePath));
        } catch (IOException e) {
            throw new OneWireDeviceError("Error while reading value: " + valuePath, e);
        }
    }

    @Override
    public void open() {
    }

    @Override
    public void close() {
    }
}
