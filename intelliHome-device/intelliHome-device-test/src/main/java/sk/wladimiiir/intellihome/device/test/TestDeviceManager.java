package sk.wladimiiir.intellihome.device.test;

import sk.wladimiiir.intellihome.device.*;
import sk.wladimiiir.intellihome.device.exception.DigitalDeviceError;
import sk.wladimiiir.intellihome.device.exception.OneWireDeviceError;
import sk.wladimiiir.intellihome.device.exception.PinException;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * @author Vladimir Hrusovsky
 */
@Named("test")
@Singleton
public class TestDeviceManager implements DeviceManager {
    private static int pinNumber = 1;

    @Override
    public OneWireDevice getOneWireDevice(String serialNumber) throws OneWireDeviceError {
        return new TestOneWireDevice(serialNumber);
    }

    @Override
    public DigitalInputPinDevice getDigitalInputPinDevice(Pin pin) throws DigitalDeviceError {
        return new TestDigitalPinDevice(pin);
    }

    @Override
    public DigitalOutputPinDevice getDigitalOutputPinDevice(Pin pin) throws DigitalDeviceError {
        return new TestDigitalPinDevice(pin);
    }

    @Override
    public Pin getPinByKey(String key) throws PinException {
        return new Pin(key, pinNumber++);
    }

    @Override
    public Pin getPinByName(String name) throws PinException {
        return new Pin(name, pinNumber++);
    }

    @Override
    public Pin getPinByGPIO(int gpio) throws PinException {
        return new Pin("ByGPIO_" + gpio, gpio);
    }

    @Override
    public void close() {
        System.out.println("Closing Device manager");
    }
}
