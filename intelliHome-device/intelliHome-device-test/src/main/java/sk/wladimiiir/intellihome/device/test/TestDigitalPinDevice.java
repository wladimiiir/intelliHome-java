package sk.wladimiiir.intellihome.device.test;

import sk.wladimiiir.intellihome.device.DigitalInputPinDevice;
import sk.wladimiiir.intellihome.device.DigitalOutputPinDevice;
import sk.wladimiiir.intellihome.device.Pin;
import sk.wladimiiir.intellihome.device.exception.DigitalDeviceError;

/**
 * @author Vladimir Hrusovsky
 */
public class TestDigitalPinDevice implements DigitalInputPinDevice, DigitalOutputPinDevice {
    private final Pin pin;
    private int value = -1;

    public TestDigitalPinDevice(Pin pin) {
        this.pin = pin;
    }

    @Override
    public Pin getPin() {
        return pin;
    }

    @Override
    public void low() throws DigitalDeviceError {
        System.out.println("Setting low on device: " + pin);
        value = LOW;
    }

    @Override
    public void high() throws DigitalDeviceError {
        System.out.println("Setting high on device: " + pin);
        value = HIGH;
    }

    @Override
    public void writeValue(int value) throws DigitalDeviceError {
        System.out.println("Writing value=" + value + " on device: " + pin);
        this.value = value;
    }

    @Override
    public int readValue() throws DigitalDeviceError {
        return value;
    }

    @Override
    public void open() {
    }

    @Override
    public void close() {
    }
}
