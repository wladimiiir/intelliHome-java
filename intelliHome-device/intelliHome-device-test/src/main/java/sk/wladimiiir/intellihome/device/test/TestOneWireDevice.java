package sk.wladimiiir.intellihome.device.test;

import sk.wladimiiir.intellihome.device.OneWireDevice;
import sk.wladimiiir.intellihome.device.exception.OneWireDeviceError;

import java.util.Random;

/**
 * @author Vladimir Hrusovsky
 */
public class TestOneWireDevice implements OneWireDevice {
    private final String serialNumber;

    public TestOneWireDevice(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Override
    public String getSerialNumber() {
        return serialNumber;
    }

    @Override
    public String readValue() throws OneWireDeviceError {
        return "YES;t=" + (25000 + new Random().nextInt(30000));
    }

    @Override
    public void open() {
    }

    @Override
    public void close() {
    }
}
