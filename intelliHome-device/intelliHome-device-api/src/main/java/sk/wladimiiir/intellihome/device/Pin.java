package sk.wladimiiir.intellihome.device;

/**
 * @author wladimiiir
 * @since 7/4/15
 */
public class Pin {
    private final String key;
    private final int number;

    public Pin(String key, int number) {
        this.key = key;
        this.number = number;
    }

    public String getKey() {
        return key;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return number + " (" + key + ")";
    }
}
