package sk.wladimiiir.intellihome.device.exception;

/**
 * @author wladimiiir
 * @since 7/4/15
 */
public class DigitalDeviceError extends Exception {
    public DigitalDeviceError(String message) {
        super(message);
    }

    public DigitalDeviceError(String message, Throwable cause) {
        super(message, cause);
    }
}
