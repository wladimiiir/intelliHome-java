package sk.wladimiiir.intellihome.device;

import sk.wladimiiir.intellihome.device.exception.DigitalDeviceError;

/**
 * @author wladimiiir
 * @since 7/4/15
 */
public interface DigitalOutputPinDevice extends Device {
    int LOW = 0;
    int HIGH = 1;

    Pin getPin();

    void low() throws DigitalDeviceError;

    void high() throws DigitalDeviceError;

    void writeValue(int value) throws DigitalDeviceError;
}
