package sk.wladimiiir.intellihome.device;

import sk.wladimiiir.intellihome.device.exception.DigitalDeviceError;

/**
 * @author wladimiiir
 * @since 7/4/15
 */
public interface DigitalInputPinDevice extends Device {
    Pin getPin();

    int readValue() throws DigitalDeviceError;
}
