package sk.wladimiiir.intellihome.device;

import sk.wladimiiir.intellihome.device.exception.OneWireDeviceError;

/**
 * @author wladimiiir
 * @since 7/4/15
 */
public interface OneWireDevice extends Device {
    String getSerialNumber();

    String readValue() throws OneWireDeviceError;
}
