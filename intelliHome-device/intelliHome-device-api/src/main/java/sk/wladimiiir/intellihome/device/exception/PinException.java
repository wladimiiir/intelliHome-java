package sk.wladimiiir.intellihome.device.exception;

/**
 * @author Vladimir Hrusovsky
 */
public class PinException extends Exception {
    public PinException() {
    }

    public PinException(String message) {
        super(message);
    }
}
