package sk.wladimiiir.intellihome.device;

import sk.wladimiiir.intellihome.device.exception.DigitalDeviceError;
import sk.wladimiiir.intellihome.device.exception.OneWireDeviceError;
import sk.wladimiiir.intellihome.device.exception.PinException;

/**
 * @author wladimiiir
 * @since 7/4/15
 */
public interface DeviceManager {
    OneWireDevice getOneWireDevice(String serialNumber) throws OneWireDeviceError;

    DigitalInputPinDevice getDigitalInputPinDevice(Pin pin) throws DigitalDeviceError;

    DigitalOutputPinDevice getDigitalOutputPinDevice(Pin pin) throws DigitalDeviceError;

    Pin getPinByKey(String key) throws PinException;

    Pin getPinByName(String name) throws PinException;

    Pin getPinByGPIO(int gpio) throws PinException;

    void close();
}
