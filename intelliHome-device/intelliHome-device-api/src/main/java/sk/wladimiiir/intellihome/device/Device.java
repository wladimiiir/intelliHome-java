package sk.wladimiiir.intellihome.device;

import sk.wladimiiir.intellihome.device.exception.DigitalDeviceError;

/**
 * @author Vladimir Hrusovsky
 */
public interface Device {
    void open() throws DigitalDeviceError;

    void close() throws DigitalDeviceError;
}
