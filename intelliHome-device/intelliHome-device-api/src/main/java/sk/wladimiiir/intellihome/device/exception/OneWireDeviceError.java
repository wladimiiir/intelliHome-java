package sk.wladimiiir.intellihome.device.exception;

/**
 * @author wladimiiir
 * @since 7/4/15
 */
public class OneWireDeviceError extends Exception {
    public OneWireDeviceError(String message) {
        super(message);
    }

    public OneWireDeviceError(String message, Throwable cause) {
        super(message, cause);
    }
}
