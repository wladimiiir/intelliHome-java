package sk.wladimiiir.intellihome.model.unit;

import sk.wladimiiir.intellihome.model.exception.UnitException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wladimiiir
 * @since 7/5/15
 */
public abstract class AbstractUnit implements Unit {
    private final String id;
    private final String name;
    private final List<UnitControl> controls = new ArrayList<>();

    private State state = State.STOPPED;
    private State stateOnNormal = null;

    public AbstractUnit(String id, String name) {
        this.id = id;
        this.name = name;
    }

    protected abstract void startUnit() throws UnitException;

    protected abstract void stopUnit() throws UnitException;

    @Override
    public String getID() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public State getState() {
        return state;
    }

    @Override
    public void setState(State state) throws UnitException {
        switch (state) {
            case STARTED:
            case STOPPED:
                resume();
                break;
            case FORCE_RUN:
                forceRun();
                break;
            case SUSPENDED:
                suspend();
                break;
        }
    }

    @Override
    public boolean start() throws UnitException {
        switch (state) {
            case STOPPED:
                startUnit();
                state = State.STARTED;
                return true;
            case STARTED:
                return true;
            case FORCE_RUN:
                stateOnNormal = State.STARTED;
                return true;
            case SUSPENDED:
                stateOnNormal = State.STARTED;
                return false;
            default:
                throw new UnitException("Unknown state of unit: " + state);
        }
    }

    @Override
    public boolean stop() throws UnitException {
        switch (state) {
            case STOPPED:
                return true;
            case STARTED:
                stopUnit();
                state = State.STOPPED;
                return true;
            case FORCE_RUN:
                stateOnNormal = State.STOPPED;
                return false;
            case SUSPENDED:
                stateOnNormal = State.STOPPED;
                return true;
            default:
                throw new UnitException("Unknown state of unit: " + state);
        }
    }

    @Override
    public void suspend() throws UnitException {
        if (stateOnNormal == null) {
            stateOnNormal = state;
        }
        final State previousState = state;
        state = State.SUSPENDED;
        if (previousState == State.FORCE_RUN || previousState == State.STARTED) {
            stopUnit();
        }
    }

    @Override
    public void forceRun() throws UnitException {
        if (stateOnNormal == null) {
            stateOnNormal = state;
        }
        final State previousState = state;
        state = State.FORCE_RUN;
        if (previousState == State.SUSPENDED || previousState == State.STOPPED) {
            startUnit();
        }
    }

    @Override
    public void resume() throws UnitException {
        if (state != State.FORCE_RUN && state != State.SUSPENDED) {
            return;
        }

        final State previousState = state;
        if (previousState == State.SUSPENDED && stateOnNormal == State.STARTED) {
            startUnit();
        } else if (previousState == State.FORCE_RUN && stateOnNormal == State.STOPPED) {
            stopUnit();
        }
        state = stateOnNormal;
        stateOnNormal = null;
    }

	@Override
	public void registerControl(UnitControl control) {
		controls.add(control);
	}

    @Override
    public List<UnitControl> getControls() {
        return controls;
    }

    protected void handleException(Exception e) {
        e.printStackTrace();
    }
}
