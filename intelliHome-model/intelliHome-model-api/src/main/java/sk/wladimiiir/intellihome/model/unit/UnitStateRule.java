package sk.wladimiiir.intellihome.model.unit;

import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
@FunctionalInterface
public interface UnitStateRule {
    Optional<Unit.State> getNewState();
}
