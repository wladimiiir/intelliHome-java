package sk.wladimiiir.intellihome.model.unit.control;

import sk.wladimiiir.intellihome.model.unit.UnitControl;

import java.time.Duration;

/**
 * @author Vladimir Hrusovsky
 */
public interface TimingUnitControl extends UnitControl {
    void startFor(Duration duration);

    Duration getRemaining();

    void cancel();
}
