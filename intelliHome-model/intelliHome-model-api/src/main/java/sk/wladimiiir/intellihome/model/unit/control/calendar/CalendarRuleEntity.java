package sk.wladimiiir.intellihome.model.unit.control.calendar;

import java.time.LocalDateTime;

/**
 * @author Vladimir Hrusovsky
 */
public interface CalendarRuleEntity {
    Long getId();

    LocalDateTime getStartTime();

    void setStartTime(LocalDateTime startTime);

    LocalDateTime getEndTime();

    void setEndTime(LocalDateTime endTime);

    String getRecurrenceRule();

    void setRecurrenceRule(String recurrenceRule);
}
