package sk.wladimiiir.intellihome.model.exception;

/**
 * @author wladimiiir
 * @since 7/5/15
 */
public class UnitException extends Exception {
    public UnitException(String message) {
        super(message);
    }

    public UnitException(String message, Throwable cause) {
        super(message, cause);
    }
}
