package sk.wladimiiir.intellihome.model.thermometer;

import sk.wladimiiir.intellihome.model.exception.ThermometerException;

/**
 * @author wladimiiir
 * @since 7/4/15
 */
public interface Thermometer {
    String getID();

    String getName();

    float getTemperature() throws ThermometerException;
}
