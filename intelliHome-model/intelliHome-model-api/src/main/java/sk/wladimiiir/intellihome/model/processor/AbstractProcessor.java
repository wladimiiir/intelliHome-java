package sk.wladimiiir.intellihome.model.processor;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author wladimiiir
 * @since 7/5/15
 */
public abstract class AbstractProcessor implements Processor {
	private static final int DEFAULT_PROCESS_PERIOD = 1000;

	private volatile Timer timer = null;
	protected volatile State state = State.STOPPED;

	protected abstract void process();

	protected void onStop() {
	}

	@Override
	public void start() {
		start(DEFAULT_PROCESS_PERIOD);
	}

	@Override
	public synchronized void start(long processPeriod) {
		if (timer != null) {
			//already started
			return;
		}
		timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				process();
			}
		}, 0, processPeriod);
		state = State.IDLE;
	}

	@Override
	public synchronized void stop() {
		if (state == State.RUNNING) {
			onStop();
		}
		timer.cancel();
		timer = null;
		state = State.STOPPED;
	}

	protected void handleException(Exception e) {
		e.printStackTrace();
	}

	public enum State {
		STOPPED,
		IDLE,
		RUNNING
	}
}
