package sk.wladimiiir.intellihome.model.unit.control.calendar;

/**
 * @author Vladimir Hrusovsky
 */
public interface UnitStateCalendarRuleEntity extends CalendarRuleEntity {
    boolean isRunning();
}
