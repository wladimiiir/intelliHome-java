package sk.wladimiiir.intellihome.model.processor;

/**
 * @author wladimiiir
 * @since 7/5/15
 */
public interface Processor {
    void start();

    void start(long processPeriod);

    void stop();
}
