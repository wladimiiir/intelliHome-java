package sk.wladimiiir.intellihome.model.temperature;

/**
 * @author wladimiiir
 * @since 7/5/15
 */
public interface TemperatureRule {
    float getMin();
    float getMax();
}
