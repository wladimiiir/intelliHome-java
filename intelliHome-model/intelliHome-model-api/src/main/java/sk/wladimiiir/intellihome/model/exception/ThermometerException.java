package sk.wladimiiir.intellihome.model.exception;

/**
 * @author wladimiiir
 * @since 7/4/15
 */
public class ThermometerException extends Exception {
    public ThermometerException(String message) {
        super(message);
    }

    public ThermometerException(String message, Throwable cause) {
        super(message, cause);
    }
}
