package sk.wladimiiir.intellihome.model.unit.control;

import sk.wladimiiir.intellihome.model.unit.UnitControl;
import sk.wladimiiir.intellihome.model.unit.control.calendar.TemperatureCalendarRuleEntity;

import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public interface TemperatureUnitControl extends UnitControl {
    String getThermometerID();

    float getMin();

    float getMax();

    void setManual(float min, float max);

    void setAuto();

    boolean isManual();

    boolean hasCalendarRules();

    List<TemperatureCalendarRuleEntity> getCalendarRules();

    void setCalendarRules(List<TemperatureCalendarRuleEntity> rules);
}
