package sk.wladimiiir.intellihome.model.time;

/**
 * @author Vladimir Hrusovsky
 */
@FunctionalInterface
public interface TimeRule {
    boolean isNowInTime();
}
