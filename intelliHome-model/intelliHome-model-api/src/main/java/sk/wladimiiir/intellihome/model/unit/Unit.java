package sk.wladimiiir.intellihome.model.unit;

import sk.wladimiiir.intellihome.model.exception.UnitException;

import java.util.List;
import java.util.ResourceBundle;

/**
 * @author wladimiiir
 * @since 7/5/15
 */
public interface Unit {
    enum State {
        STOPPED,
        STARTED,
        SUSPENDED,
        FORCE_RUN;

        private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("sk.wladimiiir.intellihome.model.locale.unit");

        public String getName() {
            return BUNDLE.getString("state." + name());
        }
    }

    String getID();

    String getName();

    State getState();

    void setState(State state) throws UnitException;

    boolean start() throws UnitException;

    boolean stop() throws UnitException;

    void suspend() throws UnitException;

    void forceRun() throws UnitException;

    void resume() throws UnitException;

	void registerControl(UnitControl control);

	List<UnitControl> getControls();
}
