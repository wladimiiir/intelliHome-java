package sk.wladimiiir.intellihome.model.unit.control;

import sk.wladimiiir.intellihome.model.unit.Unit;
import sk.wladimiiir.intellihome.model.unit.UnitControl;
import sk.wladimiiir.intellihome.model.unit.control.calendar.UnitStateCalendarRuleEntity;

import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public interface UnitStateUnitControl extends UnitControl {
    Unit getUnit();

    boolean hasCalendarRules();

    List<UnitStateCalendarRuleEntity> getCalendarRules();

    void setCalendarRules(List<UnitStateCalendarRuleEntity> rules);
}
