package sk.wladimiiir.intellihome.model.unit;

/**
 * @author Vladimir Hrusovsky
 */
public interface UnitControl {
    enum Type {
        STATE,
        TEMPERATURE,
        TIMING
    }

    String getID();

    Type getType();
}
