package sk.wladimiiir.intellihome.model.unit.control.calendar;

/**
 * @author Vladimir Hrusovsky
 */
public interface TemperatureCalendarRuleEntity extends CalendarRuleEntity {
    float getMinTemperature();

    void setMinTemperature(float minTemperature);

    float getMaxTemperature();

    void setMaxTemperature(float maxTemperature);
}
