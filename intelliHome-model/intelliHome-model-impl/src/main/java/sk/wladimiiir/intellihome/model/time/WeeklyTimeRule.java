package sk.wladimiiir.intellihome.model.time;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

/**
 * @author Vladimir Hrusovsky
 */
public class WeeklyTimeRule implements TimeRule {
    private final Map<DayOfWeek, List<DailyTimeRule.DayTimeRule>> timeRules = new HashMap<>();

    public void addTimeRule(DayOfWeek dayOfWeek, LocalTime startTime, Duration duration) {
        if (!timeRules.containsKey(dayOfWeek)) {
            timeRules.put(dayOfWeek, new ArrayList<>());
        }

        timeRules.get(dayOfWeek).add(new DailyTimeRule.DayTimeRule(startTime, duration));
    }

    @Override
    public boolean isNowInTime() {
        final DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
        return timeRules.getOrDefault(dayOfWeek, Collections.emptyList())
                .parallelStream()
                .filter(DailyTimeRule.DayTimeRule::isNow)
                .findFirst()
                .isPresent();
    }
}
