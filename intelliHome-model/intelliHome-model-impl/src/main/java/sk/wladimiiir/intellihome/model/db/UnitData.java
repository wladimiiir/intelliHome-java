package sk.wladimiiir.intellihome.model.db;

import sk.wladimiiir.intellihome.model.unit.Unit;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * @author Vladimir Hrusovsky
 */
@Entity
@Table(name = "unit_data")
public class UnitData extends BaseEntity {
    @Column(name = "unit_id")
    private String unitID;
    @Column(name = "date_time")
    private LocalDateTime dateTime;
    @Enumerated
    @Column(name = "state")
    private Unit.State state;

    public UnitData() {
    }

    public UnitData(String unitID, LocalDateTime dateTime, Unit.State state) {
        this.unitID = unitID;
        this.dateTime = dateTime;
        this.state = state;
    }

    public String getUnitID() {
        return unitID;
    }

    public void setUnitID(String unitID) {
        this.unitID = unitID;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Unit.State getState() {
        return state;
    }

    public void setState(Unit.State state) {
        this.state = state;
    }
}
