package sk.wladimiiir.intellihome.model.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * @author Vladimir Hrusovsky
 */
@Entity
@Table(name = "thermometer_data")
public class ThermometerData extends BaseEntity {
    @Column(name = "thermometer_id")
    private String thermometerID;
    @Column(name = "date_time")
    private LocalDateTime dateTime;
    @Column(name = "temperature")
    private float temperature;

    public ThermometerData() {
    }

    public ThermometerData(String thermometerID, LocalDateTime dateTime, float temperature) {
        this.thermometerID = thermometerID;
        this.dateTime = dateTime;
        this.temperature = temperature;
    }

    public String getThermometerID() {
        return thermometerID;
    }

    public void setThermometerID(String thermometerID) {
        this.thermometerID = thermometerID;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }
}
