package sk.wladimiiir.intellihome.model.unit.control;

import sk.wladimiiir.intellihome.model.temperature.TemperatureCalendarRule;
import sk.wladimiiir.intellihome.model.temperature.TemperatureRule;
import sk.wladimiiir.intellihome.model.unit.control.calendar.TemperatureCalendarRuleEntity;

import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public class TemperatureRuleControl implements TemperatureUnitControl, TemperatureRule {
    private final String id;
    private final String thermometerID;
    private final TemperatureRule temperatureRule;

    private boolean manual = false;
    private float manualMin;
    private float manualMax;

    public TemperatureRuleControl(String id, String thermometerID, TemperatureRule temperatureRule) {
        this.id = id;
        this.thermometerID = thermometerID;
        this.temperatureRule = temperatureRule;
    }

    @Override
    public String getID() {
        return id;
    }

    @Override
    public Type getType() {
        return Type.TEMPERATURE;
    }

    @Override
    public String getThermometerID() {
        return thermometerID;
    }

    @Override
    public float getMin() {
        return manual ? manualMin : temperatureRule.getMin();
    }

    @Override
    public float getMax() {
        return manual ? manualMax : temperatureRule.getMax();
    }

    @Override
    public void setManual(float min, float max) {
        manual = true;
        manualMin = min;
        manualMax = max;
    }

    @Override
    public void setAuto() {
        manual = false;
    }

    @Override
    public boolean isManual() {
        return manual;
    }

    @Override
    public boolean hasCalendarRules() {
        return temperatureRule instanceof TemperatureCalendarRule;
    }

    @Override
    public List<TemperatureCalendarRuleEntity> getCalendarRules() {
        return ((TemperatureCalendarRule) temperatureRule).getRules();
    }

    @Override
    public void setCalendarRules(List<TemperatureCalendarRuleEntity> rules) {
        ((TemperatureCalendarRule) temperatureRule).setRules(rules);
    }
}
