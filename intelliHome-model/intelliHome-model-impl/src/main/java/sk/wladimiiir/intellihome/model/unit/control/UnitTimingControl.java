package sk.wladimiiir.intellihome.model.unit.control;

import sk.wladimiiir.intellihome.common.utils.TimerUtils;
import sk.wladimiiir.intellihome.model.exception.UnitException;
import sk.wladimiiir.intellihome.model.unit.Unit;

import java.time.Duration;
import java.time.Instant;
import java.util.Timer;

/**
 * @author Vladimir Hrusovsky
 */
public class UnitTimingControl implements TimingUnitControl {
    private final String id;
    private final Unit unit;


    private Instant endTime;
    private Unit.State previousState;
    private Timer timer;

    public UnitTimingControl(String id, Unit unit) {
        this.id = id;
        this.unit = unit;
    }

    @Override
    public String getID() {
        return id;
    }

    @Override
    public Type getType() {
        return Type.TIMING;
    }

    @Override
    public void startFor(Duration duration) {
        try {
            previousState = unit.getState();
            unit.forceRun();
            endTime = Instant.now().plus(duration);
            timer = TimerUtils.startTimeout(this::cancel, duration);
        } catch (UnitException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Duration getRemaining() {
        if (endTime == null) {
            return Duration.ZERO;
        }
        return Duration.between(Instant.now(), endTime);
    }

    @Override
    public void cancel() {
        if (timer == null) {
            //nothing to cancel
            return;
        }

        timer.cancel();
        timer = null;
        endTime = null;
        try {
            unit.setState(previousState);
        } catch (UnitException e) {
            e.printStackTrace();
        }
    }
}
