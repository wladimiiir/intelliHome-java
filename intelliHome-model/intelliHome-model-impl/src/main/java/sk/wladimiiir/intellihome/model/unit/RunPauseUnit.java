package sk.wladimiiir.intellihome.model.unit;

import sk.wladimiiir.intellihome.model.exception.UnitException;

import java.time.Duration;
import java.util.Timer;

import static sk.wladimiiir.intellihome.common.utils.TimerUtils.startTimeout;

/**
 * @author wladimiiir
 * @since 7/5/15
 */
public class RunPauseUnit extends AbstractUnit {
    private final Unit controlledUnit;
    private final Duration runTime;
    private final Duration pauseTime;
    private final boolean useForceRun;

    private volatile State previousState;
    private volatile Timer timer;

    public RunPauseUnit(Unit controlledUnit, Duration runTime, Duration pauseTime) {
        this(controlledUnit, runTime, pauseTime, false);
    }

    public RunPauseUnit(Unit controlledUnit, Duration runTime, Duration pauseTime, boolean useForceRun) {
        super(controlledUnit.getID(), controlledUnit.getName());
        this.controlledUnit = controlledUnit;
        this.runTime = runTime;
        this.pauseTime = pauseTime;
        this.useForceRun = useForceRun;
    }

    @Override
    protected synchronized void startUnit() throws UnitException {
        startControlledUnit();
        timer = startTimeout(this::pause, runTime);
    }

    private void startControlledUnit() throws UnitException {
        previousState = controlledUnit.getState();
        if (useForceRun) {
            controlledUnit.forceRun();
        } else {
            controlledUnit.start();
        }
    }

    private void stopControlledUnit() throws UnitException {
        if (previousState != null) {
            controlledUnit.setState(previousState);
            previousState = null;
        }
    }

    protected synchronized void pause() {
        try {
            stopControlledUnit();
            timer = startTimeout(this::run, pauseTime);
        } catch (UnitException e) {
            handleException(e);
        }
    }

    private synchronized void run() {
        try {
            startControlledUnit();
            timer = startTimeout(this::pause, runTime);
        } catch (UnitException e) {
            handleException(e);
        }
    }

    @Override
    protected synchronized void stopUnit() throws UnitException {
        stopControlledUnit();
        timer.cancel();
    }
}
