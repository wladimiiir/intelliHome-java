package sk.wladimiiir.intellihome.model.db.query;

/**
 * @author Vladimir Hrusovsky
 */
public class MinMaxAverageEntity {
	private final int groupValue;
	private final float min;
	private final float max;
	private final float average;

	public MinMaxAverageEntity(int groupValue, float min, float max, float average) {
		this.groupValue = groupValue;
		this.min = min;
		this.max = max;
		this.average = average;
	}

	public int getGroupValue() {
		return groupValue;
	}

	public float getMin() {
		return min;
	}

	public float getMax() {
		return max;
	}

	public float getAverage() {
		return average;
	}
}
