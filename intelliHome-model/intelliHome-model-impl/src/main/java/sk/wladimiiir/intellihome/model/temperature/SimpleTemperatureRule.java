package sk.wladimiiir.intellihome.model.temperature;

/**
 * @author wladimiiir
 * @since 7/5/15
 */
public class SimpleTemperatureRule implements TemperatureRule {
    private final float min;
    private final float max;

    public SimpleTemperatureRule(float min, float max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public float getMin() {
        return min;
    }

    @Override
    public float getMax() {
        return max;
    }
}
