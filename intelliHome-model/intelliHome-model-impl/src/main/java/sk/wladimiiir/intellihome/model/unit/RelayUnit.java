package sk.wladimiiir.intellihome.model.unit;

import sk.wladimiiir.intellihome.model.exception.UnitException;
import sk.wladimiiir.intellihome.device.DigitalOutputPinDevice;
import sk.wladimiiir.intellihome.device.exception.DigitalDeviceError;

/**
 * @author wladimiiir
 * @since 7/5/15
 */
public class RelayUnit extends AbstractUnit {
    private final DigitalOutputPinDevice device;

    public RelayUnit(String id, String name, DigitalOutputPinDevice device) {
        super(id, name);
        this.device = device;
        try {
            this.device.high();
        } catch (DigitalDeviceError digitalDeviceError) {
            throw new RuntimeException(digitalDeviceError);
        }
    }

    @Override
    protected void startUnit() throws UnitException {
        try {
            device.low();
        } catch (DigitalDeviceError digitalDeviceError) {
            throw new UnitException("Cannot start relay unit on pin: " + device.getPin(), digitalDeviceError);
        }
    }

    @Override
    protected void stopUnit() throws UnitException {
        try {
            device.high();
        } catch (DigitalDeviceError digitalDeviceError) {
            throw new UnitException("Cannot stop relay unit on pin: " + device.getPin(), digitalDeviceError);
        }
    }
}
