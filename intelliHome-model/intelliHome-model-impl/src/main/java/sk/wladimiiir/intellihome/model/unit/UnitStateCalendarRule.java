package sk.wladimiiir.intellihome.model.unit;

import sk.wladimiiir.intellihome.model.unit.control.calendar.AbstractCalendarRule;
import sk.wladimiiir.intellihome.model.unit.control.calendar.UnitStateCalendarRuleEntity;

import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
public class UnitStateCalendarRule extends AbstractCalendarRule<UnitStateCalendarRuleEntity> implements UnitStateRule {
    private final UnitStateRule parentUnitStateRule;

    public UnitStateCalendarRule(UnitStateRule parentUnitStateRule) {
        this.parentUnitStateRule = parentUnitStateRule;
    }

    @Override
    public Optional<Unit.State> getNewState() {
        final Optional<UnitStateCalendarRuleEntity> rule = getRuleInNowTime();
        if (!rule.isPresent()) {
            return parentUnitStateRule.getNewState();
        }

        return Optional.of(rule.get().isRunning() ? Unit.State.FORCE_RUN : Unit.State.SUSPENDED);
    }
}
