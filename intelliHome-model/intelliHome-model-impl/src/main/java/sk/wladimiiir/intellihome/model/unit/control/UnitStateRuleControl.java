package sk.wladimiiir.intellihome.model.unit.control;

import sk.wladimiiir.intellihome.model.unit.Unit;
import sk.wladimiiir.intellihome.model.unit.UnitStateCalendarRule;
import sk.wladimiiir.intellihome.model.unit.UnitStateRule;
import sk.wladimiiir.intellihome.model.unit.control.calendar.UnitStateCalendarRuleEntity;

import java.util.List;
import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
public class UnitStateRuleControl implements UnitStateUnitControl {
    private final Unit unit;
    private final UnitStateRule rule;

    public UnitStateRuleControl(Unit unit, UnitStateRule rule) {
        this.unit = unit;
        this.rule = rule;
    }

    @Override
    public String getID() {
        return unit.getID();
    }

    @Override
    public Type getType() {
        return Type.STATE;
    }

    @Override
    public Unit getUnit() {
        return unit;
    }

    @Override
    public boolean hasCalendarRules() {
        return rule instanceof UnitStateCalendarRule;
    }

    public Optional<Unit.State> getNewState() {
        return rule.getNewState();
    }

    @Override
    public List<UnitStateCalendarRuleEntity> getCalendarRules() {
        return ((UnitStateCalendarRule) rule).getRules();
    }

    @Override
    public void setCalendarRules(List<UnitStateCalendarRuleEntity> rules) {
        ((UnitStateCalendarRule) rule).setRules(rules);
    }
}
