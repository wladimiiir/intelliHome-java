package sk.wladimiiir.intellihome.model.db.query;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * @author Vladimir Hrusovsky
 */
public class DurationEntity {
	private final LocalDateTime groupTime;
	private final Duration duration;

	public DurationEntity(LocalDateTime groupTime, Duration duration) {
		this.groupTime = groupTime;
		this.duration = duration;
	}

	public LocalDateTime getGroupTime() {
		return groupTime;
	}

	public Duration getDuration() {
		return duration;
	}
}
