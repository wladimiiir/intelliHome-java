package sk.wladimiiir.intellihome.model.processor;

import java.util.Optional;

import sk.wladimiiir.intellihome.model.exception.ThermometerException;
import sk.wladimiiir.intellihome.model.exception.UnitException;
import sk.wladimiiir.intellihome.model.temperature.TemperatureRule;
import sk.wladimiiir.intellihome.model.thermometer.Thermometer;
import sk.wladimiiir.intellihome.model.unit.Unit;
import sk.wladimiiir.intellihome.model.unit.control.UnitStateRuleControl;

/**
 * @author wladimiiir
 * @since 7/5/15
 */
public class Thermostat extends AbstractProcessor {
    private final Thermometer thermometer;
    private final TemperatureRule temperatureRule;
    private final ControlType controlType;
    private final Unit controlledUnit;

    public Thermostat(Thermometer thermometer, TemperatureRule temperatureRule, ControlType controlType, Unit controlledUnit) {
        this.thermometer = thermometer;
        this.temperatureRule = temperatureRule;
        this.controlType = controlType;
        this.controlledUnit = controlledUnit;
    }

    @Override
    protected void onStop() {
        try {
            controlledUnit.stop();
        } catch (UnitException e) {
            handleException(e);
        }
    }

    @Override
    protected void process() {
        try {
            final float temperature = thermometer.getTemperature();

            switch (controlType) {
                case HEATING:
                    processHeating(temperature);
                    break;
                case COOLING:
                    processCooling(temperature);
                    break;
                default:
                    break;
            }
        } catch (ThermometerException | UnitException e) {
            handleException(e);
        }
    }

    private void processHeating(float temperature) throws UnitException {
        final Optional<Optional<Unit.State>> ruleStateOptional = controlledUnit.getControls().stream()
                .filter(unitControl -> unitControl instanceof UnitStateRuleControl)
                .filter(unitControl -> ((UnitStateRuleControl) unitControl).hasCalendarRules())
                .map(unitControl -> ((UnitStateRuleControl) unitControl).getNewState())
                .filter(Optional::isPresent)
                .findFirst();

        switch (state) {
            case IDLE:
                if (ruleStateOptional.isPresent()) {
                    switch (ruleStateOptional.get().get()) {
                        case FORCE_RUN:
                            controlledUnit.start();
                            state = State.RUNNING;
                            break;
                    }
                    break;
                }

                if (temperature < temperatureRule.getMin()) {
                    controlledUnit.start();
                    state = State.RUNNING;
                }
                break;
            case RUNNING:
                if (ruleStateOptional.isPresent()) {
                    switch (ruleStateOptional.get().get()) {
                        case SUSPENDED:
                            controlledUnit.stop();
                            state = State.IDLE;
                            break;
                    }
                    break;
                }

                if (temperature > temperatureRule.getMax()) {
                    controlledUnit.stop();
                    state = State.IDLE;
                }
                break;
            default:
                break;
        }
    }

    private void processCooling(float temperature) throws UnitException {
        switch (state) {
            case IDLE:
                if (temperature > temperatureRule.getMax()) {
                    controlledUnit.start();
                    state = State.RUNNING;
                }
                break;
            case RUNNING:
                if (temperature < temperatureRule.getMin()) {
                    controlledUnit.stop();
                    state = State.IDLE;
                }
                break;
            default:
                break;
        }
    }

    public enum ControlType {
        HEATING,
        COOLING
    }
}
