package sk.wladimiiir.intellihome.model.unit;

import sk.wladimiiir.intellihome.model.exception.UnitException;
import sk.wladimiiir.intellihome.model.processor.Processor;
import sk.wladimiiir.intellihome.model.processor.ThreeWayValveController;

import java.time.Duration;

/**
 * @author wladimiiir
 * @since 7/5/15
 */
public class FloorHeatingUnit extends AbstractUnit {
    private final Unit pumpUnit;
    private final ThreeWayValveController threeWayValveController;
    private final Processor electricHeaterProcessor;
    private final Unit electricHeaterPumpUnit;

    public FloorHeatingUnit(String id, String name, Unit pumpUnit, ThreeWayValveController threeWayValveController, Processor electricHeaterProcessor, RelayUnit electricHeaterPumpUnit) {
        super(id, name);
        this.pumpUnit = pumpUnit;
        this.threeWayValveController = threeWayValveController;
        this.electricHeaterProcessor = electricHeaterProcessor;
        this.electricHeaterPumpUnit = new RunPauseUnit(electricHeaterPumpUnit, Duration.ofSeconds(5), Duration.ofMinutes(10), true);
    }

    @Override
    protected void startUnit() throws UnitException {
        pumpUnit.start();
        threeWayValveController.start();
        electricHeaterProcessor.start();
        electricHeaterPumpUnit.start();
    }

    @Override
    protected void stopUnit() throws UnitException {
        pumpUnit.stop();
        threeWayValveController.stop();
        electricHeaterProcessor.stop();
        electricHeaterPumpUnit.stop();
    }
}
