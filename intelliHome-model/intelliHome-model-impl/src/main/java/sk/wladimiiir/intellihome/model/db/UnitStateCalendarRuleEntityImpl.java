package sk.wladimiiir.intellihome.model.db;

import sk.wladimiiir.intellihome.model.unit.control.calendar.UnitStateCalendarRuleEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * @author Vladimir Hrusovsky
 */
@Entity
@Table(name = "state_calendar_rule")
public class UnitStateCalendarRuleEntityImpl extends BaseEntity implements UnitStateCalendarRuleEntity {
    @Column(name = "parent_id", nullable = false)
    private String parentID;
    @Column(name = "start_time")
    private LocalDateTime startTime;
    @Column(name = "end_time")
    private LocalDateTime endTime;
    @Column(name = "recurrence_rule", nullable = true)
    private String recurrenceRule;
    @Column(name = "running")
    private boolean running;

    public UnitStateCalendarRuleEntityImpl() {
    }

    public UnitStateCalendarRuleEntityImpl(LocalDateTime startTime, LocalDateTime endTime, String recurrenceRule, boolean running) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.recurrenceRule = recurrenceRule;
        this.running = running;
    }

    public String getParentID() {
        return parentID;
    }

    public void setParentID(String parentID) {
        this.parentID = parentID;
    }

    @Override
    public LocalDateTime getStartTime() {
        return startTime;
    }

    @Override
    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    @Override
    public LocalDateTime getEndTime() {
        return endTime;
    }

    @Override
    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    @Override
    public String getRecurrenceRule() {
        return recurrenceRule;
    }

    @Override
    public void setRecurrenceRule(String recurrenceRule) {
        this.recurrenceRule = recurrenceRule;
    }

    @Override
    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }
}
