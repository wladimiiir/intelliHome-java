package sk.wladimiiir.intellihome.model.time;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public class DailyTimeRule implements TimeRule {
    private final List<DayTimeRule> dayTimeRules = new ArrayList<>();

    public void addDayTimeRule(LocalTime startTime, Duration duration) {
        dayTimeRules.add(new DayTimeRule(startTime, duration));
    }

    @Override
    public boolean isNowInTime() {
        return dayTimeRules.parallelStream()
                .filter(DayTimeRule::isNow)
                .findFirst().isPresent();
    }

    public static class DayTimeRule {
        private final LocalTime startTime;
        private final LocalTime endTime;

        public DayTimeRule(LocalTime startTime, Duration duration) {
            this.startTime = startTime;
            this.endTime = startTime.plus(duration);
        }

        public boolean isNow() {
            final LocalTime now = LocalTime.now();
            return !startTime.isAfter(now) && !endTime.isBefore(now);
        }
    }

}
