package sk.wladimiiir.intellihome.model.processor;

import sk.wladimiiir.intellihome.common.utils.TimerUtils;
import sk.wladimiiir.intellihome.model.exception.ThermometerException;
import sk.wladimiiir.intellihome.model.exception.UnitException;
import sk.wladimiiir.intellihome.model.temperature.TemperatureRule;
import sk.wladimiiir.intellihome.model.thermometer.Thermometer;
import sk.wladimiiir.intellihome.model.unit.RelayUnit;

import java.time.Duration;

/**
 * @author wladimiiir
 * @since 7/5/15
 */
public class ThreeWayValveController extends AbstractProcessor {
    private static final Duration RELAY_RUN_TIME = Duration.ofSeconds(2);
    private static final Duration AFTER_RUN_WAIT_TIME = Duration.ofSeconds(5);
    private static final int VALVE_MAX_POSITION_DIFFERENCE = 150;

    private final Thermometer thermometer;
    private final TemperatureRule temperatureRule;
    private final RelayUnit lowerUnit;
    private final RelayUnit raiseUnit;

    private volatile int valvePosition = 0;

    public ThreeWayValveController(Thermometer thermometer, TemperatureRule temperatureRule, RelayUnit lowerUnit, RelayUnit raiseUnit) {
        this.thermometer = thermometer;
        this.temperatureRule = temperatureRule;
        this.lowerUnit = lowerUnit;
        this.raiseUnit = raiseUnit;
    }

    @Override
    protected synchronized void process() {
        try {
            final float temperature = thermometer.getTemperature();

            switch (state) {
                case IDLE:
                    if (temperature < temperatureRule.getMin()) {
                        raise();
                    } else if (temperature > temperatureRule.getMax()) {
                        lower();
                    }
                    break;
                case RUNNING:
                case STOPPED:
                    break;
            }
        } catch (ThermometerException | UnitException e) {
            handleException(e);
        }
    }

    private synchronized void lower() throws UnitException {
        if (valvePosition <= -VALVE_MAX_POSITION_DIFFERENCE) {
            return;
        }

        lowerUnit.start();
        valvePosition--;
        state = State.RUNNING;
        TimerUtils.startTimeout(this::stopLowerUnit, RELAY_RUN_TIME);
        TimerUtils.startTimeout(this::setIdle, RELAY_RUN_TIME.plus(AFTER_RUN_WAIT_TIME));
    }

    private synchronized void stopLowerUnit() {
        try {
            lowerUnit.stop();
        } catch (UnitException e) {
            handleException(e);
        }
    }

    private synchronized void raise() throws UnitException {
        if (valvePosition >= VALVE_MAX_POSITION_DIFFERENCE) {
            return;
        }

        raiseUnit.start();
        valvePosition++;
        state = State.RUNNING;
        TimerUtils.startTimeout(this::stopRaiseUnit, RELAY_RUN_TIME);
        TimerUtils.startTimeout(this::setIdle, RELAY_RUN_TIME.plus(AFTER_RUN_WAIT_TIME));
    }

    private synchronized void stopRaiseUnit() {
        try {
            raiseUnit.stop();
        } catch (UnitException e) {
            handleException(e);
        }
    }

    private synchronized void setIdle() {
        if (state == State.STOPPED) {
            return;
        }
        state = State.IDLE;
    }
}
