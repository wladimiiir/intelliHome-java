package sk.wladimiiir.intellihome.model.db.query;

import sk.wladimiiir.intellihome.model.unit.Unit;

import java.time.LocalDateTime;

/**
 * @author Vladimir Hrusovsky
 */
public class UnitDataEntity {
	private final LocalDateTime fromTime;
	private final LocalDateTime toTime;
	private final Unit.State state;

	public UnitDataEntity(LocalDateTime fromTime, LocalDateTime toTime, Unit.State state) {
		this.fromTime = fromTime;
		this.toTime = toTime;
		this.state = state;
	}

	public LocalDateTime getFromTime() {
		return fromTime;
	}

	public LocalDateTime getToTime() {
		return toTime;
	}

	public Unit.State getState() {
		return state;
	}
}
