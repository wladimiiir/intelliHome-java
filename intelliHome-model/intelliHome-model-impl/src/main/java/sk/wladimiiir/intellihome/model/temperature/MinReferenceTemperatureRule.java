package sk.wladimiiir.intellihome.model.temperature;

import sk.wladimiiir.intellihome.model.exception.ThermometerException;
import sk.wladimiiir.intellihome.model.thermometer.Thermometer;

/**
 * @author Vladimir Hrusovsky
 */
public class MinReferenceTemperatureRule implements TemperatureRule {
    private final TemperatureRule parentRule;
    private final Thermometer minReferenceThermometer;

    public MinReferenceTemperatureRule(TemperatureRule parentRule, Thermometer referenceThermometer) {
        this.parentRule = parentRule;
        this.minReferenceThermometer = referenceThermometer;
    }

    private boolean isOverridden() {
        try {
            return minReferenceThermometer.getTemperature() > parentRule.getMin();
        } catch (ThermometerException e) {
            return false;
        }
    }

    @Override
    public float getMin() {
        try {
            if (isOverridden()) {
                return minReferenceThermometer.getTemperature() + 0.5f;
            } else {
                return parentRule.getMin();
            }
        } catch (ThermometerException e) {
            return parentRule.getMin();
        }
    }

    @Override
    public float getMax() {
        try {
            if (isOverridden()) {
                return minReferenceThermometer.getTemperature() + 0.5f + (parentRule.getMax() - parentRule.getMin());
            } else {
                return parentRule.getMax();
            }
        } catch (ThermometerException e) {
            return parentRule.getMax();
        }
    }
}
