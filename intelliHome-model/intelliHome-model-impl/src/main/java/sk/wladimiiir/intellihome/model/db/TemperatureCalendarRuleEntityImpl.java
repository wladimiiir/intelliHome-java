package sk.wladimiiir.intellihome.model.db;

import sk.wladimiiir.intellihome.model.unit.control.calendar.TemperatureCalendarRuleEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * @author Vladimir Hrusovsky
 */
@Entity
@Table(name = "temperature_calendar_rule")
public class TemperatureCalendarRuleEntityImpl extends BaseEntity implements TemperatureCalendarRuleEntity {
    @Column(name = "parent_id", nullable = false)
    private String parentID;
    @Column(name = "start_time")
    private LocalDateTime startTime;
    @Column(name = "end_time")
    private LocalDateTime endTime;
    @Column(name = "recurrence_rule", nullable = true)
    private String recurrenceRule;
    @Column(name = "min_temp")
    private float minTemperature;
    @Column(name = "max_temp")
    private float maxTemperature;

    public TemperatureCalendarRuleEntityImpl() {
    }

    public TemperatureCalendarRuleEntityImpl(LocalDateTime startTime, LocalDateTime endTime, String recurrenceRule, float minTemperature, float maxTemperature) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.recurrenceRule = recurrenceRule;
        this.minTemperature = minTemperature;
        this.maxTemperature = maxTemperature;
    }

    public String getParentID() {
        return parentID;
    }

    public void setParentID(String parentID) {
        this.parentID = parentID;
    }

    @Override
    public LocalDateTime getStartTime() {
        return startTime;
    }

    @Override
    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    @Override
    public LocalDateTime getEndTime() {
        return endTime;
    }

    @Override
    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    @Override
    public String getRecurrenceRule() {
        return recurrenceRule;
    }

    @Override
    public void setRecurrenceRule(String recurrenceRule) {
        this.recurrenceRule = recurrenceRule;
    }

    @Override
    public float getMinTemperature() {
        return minTemperature;
    }

    @Override
    public void setMinTemperature(float minTemperature) {
        this.minTemperature = minTemperature;
    }

    @Override
    public float getMaxTemperature() {
        return maxTemperature;
    }

    @Override
    public void setMaxTemperature(float maxTemperature) {
        this.maxTemperature = maxTemperature;
    }
}
