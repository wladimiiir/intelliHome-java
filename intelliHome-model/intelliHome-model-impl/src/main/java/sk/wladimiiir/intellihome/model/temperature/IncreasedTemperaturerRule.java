package sk.wladimiiir.intellihome.model.temperature;

/**
 * @author Vladimir Hrusovsky
 */
public class IncreasedTemperaturerRule implements TemperatureRule {
    private final TemperatureRule normalTemperatureRule;
    private final float increase;

    public IncreasedTemperaturerRule(TemperatureRule normalTemperatureRule, float increase) {
        this.normalTemperatureRule = normalTemperatureRule;
        this.increase = increase;
    }


    @Override
    public float getMin() {
        return normalTemperatureRule.getMin() + increase;
    }

    @Override
    public float getMax() {
        return normalTemperatureRule.getMax() + increase;
    }
}
