package sk.wladimiiir.intellihome.model.db;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Base class for all other entities.
 */
@MappedSuperclass
public abstract class BaseEntity {

    /**
     * @see #getId()
     */
    private Long id;

    /**
     * Constructor.
     */
    public BaseEntity() {
    }

    /**
     * @return Identity of entity.
     */
    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    /**
     * @param id {@link #getId()}
     */
    public void setId(Long id) {
        this.id = id;
    }

}
