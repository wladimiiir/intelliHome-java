package sk.wladimiiir.intellihome.model.temperature;

import sk.wladimiiir.intellihome.model.unit.control.calendar.AbstractCalendarRule;
import sk.wladimiiir.intellihome.model.unit.control.calendar.TemperatureCalendarRuleEntity;

import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
public class TemperatureCalendarRule extends AbstractCalendarRule<TemperatureCalendarRuleEntity> implements TemperatureRule {
    private final TemperatureRule parentTemperatureRule;

    public TemperatureCalendarRule(TemperatureRule parentTemperatureRule) {
        this.parentTemperatureRule = parentTemperatureRule;
    }

    private Optional<Float> getRuleInNowMinTemperature() {
        final Optional<TemperatureCalendarRuleEntity> rule = getRuleInNowTime();
        return Optional.ofNullable(rule.isPresent() ? rule.get().getMinTemperature() : null);
    }

    private Optional<Float> getRuleInNowMaxTemperature() {
        final Optional<TemperatureCalendarRuleEntity> rule = getRuleInNowTime();
        return Optional.ofNullable(rule.isPresent() ? rule.get().getMaxTemperature() : null);
    }

    @Override
    public float getMin() {
        return getRuleInNowMinTemperature().orElse(parentTemperatureRule.getMin());
    }

    @Override
    public float getMax() {
        return getRuleInNowMaxTemperature().orElse(parentTemperatureRule.getMax());
    }
}
