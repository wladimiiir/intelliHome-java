package sk.wladimiiir.intellihome.service.unit;

import sk.wladimiiir.intellihome.model.unit.UnitStateCalendarRule;
import sk.wladimiiir.intellihome.model.unit.UnitStateRule;
import sk.wladimiiir.intellihome.model.unit.control.calendar.UnitStateCalendarRuleEntity;
import sk.wladimiiir.intellihome.service.unit.control.UnitStateCalendarRuleService;

import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public class PersistedUnitStateCalendarRule extends UnitStateCalendarRule {
    private final String parentID;
    private final UnitStateCalendarRuleService ruleService;

    public PersistedUnitStateCalendarRule(String parentID, UnitStateRule parentRule, UnitStateCalendarRuleService ruleService) {
        super(parentRule);
        this.parentID = parentID;
        this.ruleService = ruleService;
        loadRules();
    }

    private void loadRules() {
        final List<UnitStateCalendarRuleEntity> rules = ruleService.getRules(parentID);
        super.setRules(rules);
    }

    @Override
    public void setRules(List<UnitStateCalendarRuleEntity> rules) {
        ruleService.save(parentID, rules);
        super.setRules(rules);
    }
}
