package sk.wladimiiir.intellihome.service.processor;

import sk.wladimiiir.intellihome.device.DeviceManager;
import sk.wladimiiir.intellihome.model.processor.Processor;
import sk.wladimiiir.intellihome.model.processor.Thermostat;
import sk.wladimiiir.intellihome.model.temperature.MinReferenceTemperatureRule;
import sk.wladimiiir.intellihome.model.temperature.SimpleTemperatureRule;
import sk.wladimiiir.intellihome.model.temperature.TemperatureRule;
import sk.wladimiiir.intellihome.model.unit.Unit;
import sk.wladimiiir.intellihome.model.unit.control.TemperatureRuleControl;
import sk.wladimiiir.intellihome.service.temperature.PersistedTemperatureCalendarRule;
import sk.wladimiiir.intellihome.service.thermometer.ThermometerDefinition;
import sk.wladimiiir.intellihome.service.thermometer.ThermometerService;
import sk.wladimiiir.intellihome.service.unit.UnitDefinition;
import sk.wladimiiir.intellihome.service.unit.UnitService;
import sk.wladimiiir.intellihome.service.unit.control.TemperatureCalendarRuleService;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.*;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
public class ProcessorServiceImpl implements ProcessorService {
    private final List<Processor> processors = new ArrayList<>();

    @Inject
    private volatile DeviceManager deviceManager;
    @Inject
    private volatile UnitService unitService;
    @Inject
    private volatile ThermometerService thermometerService;
    @Inject
    private volatile TemperatureCalendarRuleService temperatureCalendarRuleService;


    @PostConstruct
    private void init() {
        processors.add(createFireplaceExchangerProcessor());
        processors.add(createBedroomThermostat());
        processors.add(createElectricHeater());
    }

    private Processor createElectricHeater() {
        final Unit electricHeaterUnit = unitService.getUnit(UnitDefinition.ELECTRIC_HEATER.getID()).get();
        final TemperatureRuleControl temperatureRule = new TemperatureRuleControl(UUID.randomUUID().toString(), ThermometerDefinition.TANK_MIDDLE.getID(), createElectricHeaterTemperatureRule());

        final Processor electricHeater = new Thermostat(
                thermometerService.getThermometer(ThermometerDefinition.TANK_MIDDLE.getID()).get(),
                temperatureRule,
                Thermostat.ControlType.HEATING,
                electricHeaterUnit
        );

        electricHeaterUnit.registerControl(temperatureRule);

        return electricHeater;
    }

    private TemperatureRule createElectricHeaterTemperatureRule() {
        return new SimpleTemperatureRule(25, 30);
    }

    private Processor createFireplaceExchangerProcessor() {
        final Unit fireplaceExchangerUnit = unitService.getUnit(UnitDefinition.FIREPLACE_EXCHANGER_PUMP.getID()).get();
        final TemperatureRuleControl temperatureRule = new TemperatureRuleControl(UUID.randomUUID().toString(), ThermometerDefinition.FIREPLACE.getID(), createFireplaceExchangerTemperatureRule());

        fireplaceExchangerUnit.registerControl(temperatureRule);

        return new Thermostat(
                thermometerService.getThermometer(ThermometerDefinition.FIREPLACE.getID()).get(),
                new MinReferenceTemperatureRule(temperatureRule, thermometerService.getThermometer(ThermometerDefinition.TANK_TOP.getID()).get()),
                Thermostat.ControlType.COOLING,
                fireplaceExchangerUnit
        );
    }

    private TemperatureRule createFireplaceExchangerTemperatureRule() {
        return new SimpleTemperatureRule(55, 60);
    }

    private Processor createBedroomThermostat() {
        final Unit floorHeatingUnit = unitService.getUnit(UnitDefinition.FLOOR_HEATING.getID()).get();
        final String temperatureControlID = UnitDefinition.FLOOR_HEATING.getID() + "_" + ThermometerDefinition.BEDROOM.getID();
        final TemperatureRuleControl temperatureRule = new TemperatureRuleControl(temperatureControlID, ThermometerDefinition.BEDROOM.getID(), createBedroomTemperatureRule(temperatureControlID));

        final Processor bedroomThermostat = new Thermostat(
                thermometerService.getThermometer(ThermometerDefinition.BEDROOM.getID()).get(),
                temperatureRule,
                Thermostat.ControlType.HEATING,
                floorHeatingUnit
        );

        floorHeatingUnit.registerControl(temperatureRule);

        return bedroomThermostat;
    }

    private TemperatureRule createBedroomTemperatureRule(String temperatureControlID) {
        final SimpleTemperatureRule simpleTemperatureRule = new SimpleTemperatureRule(21.5f, 22f);
        return new PersistedTemperatureCalendarRule(temperatureControlID, simpleTemperatureRule, temperatureCalendarRuleService);
    }

    @Override
    public Collection<Processor> getProcessors() {
        return Collections.unmodifiableList(processors);
    }
}
