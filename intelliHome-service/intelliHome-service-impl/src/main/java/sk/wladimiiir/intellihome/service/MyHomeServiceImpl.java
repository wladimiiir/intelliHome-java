package sk.wladimiiir.intellihome.service;

import sk.wladimiiir.intellihome.device.DeviceManager;
import sk.wladimiiir.intellihome.model.processor.Processor;
import sk.wladimiiir.intellihome.service.notification.NotificationService;
import sk.wladimiiir.intellihome.service.processor.ProcessorService;
import sk.wladimiiir.intellihome.service.unit.UnitDataService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

/**
 * @author wladimiiir
 * @since 7/5/15
 */
@Named
@Singleton
public class MyHomeServiceImpl implements MyHomeService {
    @Inject
    private volatile ProcessorService processorService;
    @Inject
    private volatile DeviceManager deviceManager;
    @Inject
    private volatile UnitDataService unitDataService;
    @Inject
    private volatile NotificationService notificationService;

    @Override
    public void start() {
        processorService.getProcessors().forEach(Processor::start);
        notificationService.applicationStarted();
    }

    @Override
    public void stop() {
        processorService.getProcessors().forEach(Processor::stop);
        unitDataService.close();
        deviceManager.close();
        notificationService.applicationStopped();
    }
}
