package sk.wladimiiir.intellihome.service.unit.control;

import sk.wladimiiir.intellihome.dao.TemperatureCalendarRuleDAO;
import sk.wladimiiir.intellihome.model.unit.control.calendar.TemperatureCalendarRuleEntity;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
public class TemperatureCalendarRuleServiceImpl implements TemperatureCalendarRuleService {
    @Inject
    private volatile TemperatureCalendarRuleDAO temperatureCalendarRuleDAO;

    @Override
    public void save(String parentID, List<TemperatureCalendarRuleEntity> rules) {
        temperatureCalendarRuleDAO.save(parentID, rules);
    }

    @Override
    public List<TemperatureCalendarRuleEntity> getRules(String parentID) {
        return temperatureCalendarRuleDAO.getRules(parentID);
    }
}
