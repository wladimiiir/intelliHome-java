package sk.wladimiiir.intellihome.service.unit;

import java.util.ResourceBundle;

/**
 * @author Vladimir Hrusovsky
 */
public enum UnitDefinition {
    FLOOR_HEATING("floorHeating"),
    ELECTRIC_HEATER("electricHeater"),
    FIREPLACE_EXCHANGER_PUMP("fireplaceExchanger"),
    THREE_WAY_VALVE_LOWER("threeWayValveLower"),
    THREE_WAY_VALVE_RAISE("threeWayValveRaise");

    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("sk.wladimiiir.intellihome.model.locale.unit");
    private final String id;

    UnitDefinition(String id) {
        this.id = id;
    }

    public String getID() {
        return id;
    }

    public String getName() {
        return BUNDLE.getString("unit." + id);
    }
}
