package sk.wladimiiir.intellihome.service.temperature;

import sk.wladimiiir.intellihome.model.temperature.TemperatureCalendarRule;
import sk.wladimiiir.intellihome.model.unit.control.calendar.TemperatureCalendarRuleEntity;
import sk.wladimiiir.intellihome.model.temperature.TemperatureRule;
import sk.wladimiiir.intellihome.service.unit.control.TemperatureCalendarRuleService;

import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public class PersistedTemperatureCalendarRule extends TemperatureCalendarRule {
    private final String parentID;
    private final TemperatureCalendarRuleService ruleService;

    public PersistedTemperatureCalendarRule(String parentID, TemperatureRule parentTemperatureRule, TemperatureCalendarRuleService ruleService) {
        super(parentTemperatureRule);
        this.parentID = parentID;
        this.ruleService = ruleService;
        loadRules();
    }

    private void loadRules() {
        final List<TemperatureCalendarRuleEntity> rules = ruleService.getRules(parentID);
        super.setRules(rules);
    }

    @Override
    public void setRules(List<TemperatureCalendarRuleEntity> rules) {
        ruleService.save(parentID, rules);
        super.setRules(rules);
    }
}
