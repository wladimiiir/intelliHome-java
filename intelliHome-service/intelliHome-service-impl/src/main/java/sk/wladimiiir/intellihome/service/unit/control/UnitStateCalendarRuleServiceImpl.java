package sk.wladimiiir.intellihome.service.unit.control;

import sk.wladimiiir.intellihome.dao.StateCalendarRuleDAO;
import sk.wladimiiir.intellihome.model.unit.control.calendar.UnitStateCalendarRuleEntity;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
public class UnitStateCalendarRuleServiceImpl implements UnitStateCalendarRuleService {
    @Inject
    private volatile StateCalendarRuleDAO stateCalendarRuleDAO;

    @Override
    public void save(String parentID, List<UnitStateCalendarRuleEntity> rules) {
        stateCalendarRuleDAO.save(parentID, rules);
    }

    @Override
    public List<UnitStateCalendarRuleEntity> getRules(String parentID) {
        return stateCalendarRuleDAO.getRules(parentID);
    }
}
