package sk.wladimiiir.intellihome.service.thermometer;

import sk.wladimiiir.intellihome.device.DeviceManager;
import sk.wladimiiir.intellihome.device.exception.OneWireDeviceError;
import sk.wladimiiir.intellihome.model.themometer.DS18B20Thermometer;
import sk.wladimiiir.intellihome.model.thermometer.Thermometer;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.*;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
public class ThermometerServiceImpl implements ThermometerService {
    private final Map<ThermometerDefinition, Thermometer> thermometers = new LinkedHashMap<>();

    @Inject
    private volatile DeviceManager deviceManager;

    @PostConstruct
    private void init() throws OneWireDeviceError {
        registerThermometer(ThermometerDefinition.BEDROOM, "28-00000555c393");
        registerThermometer(ThermometerDefinition.OUTSIDE, "28-000005384546");
        registerThermometer(ThermometerDefinition.KITCHEN, "28-000005558ca2");
        registerThermometer(ThermometerDefinition.FIREPLACE, "28-000005306053");
        registerThermometer(ThermometerDefinition.FLOOR_HEATING_IN, "28-0000055592d5");
        registerThermometer(ThermometerDefinition.TANK_TOP, "28-0000053875d8");
        registerThermometer(ThermometerDefinition.TANK_MIDDLE, "28-00000555e30a");
        registerThermometer(ThermometerDefinition.TANK_BOTTOM, "28-000005569310");
    }

    private void registerThermometer(ThermometerDefinition definition, String serialNumber) throws OneWireDeviceError {
        thermometers.put(definition, new DS18B20Thermometer(
                definition.getID(),
                definition.getName(),
                deviceManager.getOneWireDevice(serialNumber)
        ));
    }

    @Override
    public List<Thermometer> getThermometers() {
        return new ArrayList<>(thermometers.values());
    }

    @Override
    public Optional<Thermometer> getThermometer(String id) {
        final Optional<ThermometerDefinition> foundKey = thermometers.keySet().parallelStream()
                .filter(key -> key.getID().equals(id))
                .findFirst();
        if (foundKey.isPresent()) {
            return Optional.of(thermometers.get(foundKey.get()));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<String> getThermometerName(String id) {
        final Optional<ThermometerDefinition> foundKey = thermometers.keySet().parallelStream()
                .filter(key -> key.getID().equals(id))
                .findFirst();
        if (foundKey.isPresent()) {
            return Optional.of(foundKey.get().getName());
        } else {
            return Optional.empty();
        }
    }
}
