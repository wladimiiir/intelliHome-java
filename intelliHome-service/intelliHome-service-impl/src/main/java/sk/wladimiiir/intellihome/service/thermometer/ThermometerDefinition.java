package sk.wladimiiir.intellihome.service.thermometer;

import java.util.ResourceBundle;

/**
 * @author Vladimir Hrusovsky
 */
public enum ThermometerDefinition {
    BEDROOM("bedroom"),
    KITCHEN("kitchen"),
    OUTSIDE("outside"),
    FIREPLACE("fireplace"),
    FLOOR_HEATING_IN("floorHeatingIn"),
    TANK_TOP("tankTop"),
    TANK_MIDDLE("tankMiddle"),
    TANK_BOTTOM("tankBottom");

    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("sk.wladimiiir.intellihome.model.locale.thermometer");
    private final String id;

    ThermometerDefinition(String id) {
        this.id = id;
    }

    public String getID() {
        return id;
    }

    public String getName() {
        return BUNDLE.getString("thermometer." + id);
    }
}
