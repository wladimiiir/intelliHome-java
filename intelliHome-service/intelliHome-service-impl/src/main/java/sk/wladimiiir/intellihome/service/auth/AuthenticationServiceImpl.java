package sk.wladimiiir.intellihome.service.auth;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
public class AuthenticationServiceImpl implements AuthenticationService {
    private static final byte[] PASSWORD_HASH = new byte[]{
            70, 115, -106, -15, 50, 63, -37, 1, 56, 118, 70, -53, -82, -66, -2, -78, 120, -77, 13, -115, 72, -28, 54, 39, 21, 107, 47, -29, 22, -63, -38, -113
    };

    @Inject
    private volatile Provider<AuthSession> authSessionProvider;

    @Override
    public boolean authenticate(String password) {
        final boolean authenticated = isValidPassword(password);
        authSessionProvider.get().setAuthenticated(authenticated);
        return authenticated;
    }

    private boolean isValidPassword(String password) {
        try {
            final byte[] passwordHash = MessageDigest.getInstance("SHA-256").digest(password.getBytes());
            return Arrays.equals(PASSWORD_HASH, passwordHash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean isAuthenticated(HttpServletRequest request) {
        return isSessionAuthenticated() || isRequestAuthenticated(request);
    }

    private boolean isSessionAuthenticated() {
        return authSessionProvider.get().isAuthenticated();
    }

    private boolean isRequestAuthenticated(HttpServletRequest request) {
        if (request == null) {
            return false;
        }

        try {
            return request.getRemoteAddr().startsWith("192.168.1.")
                    || InetAddress.getByName(request.getRemoteAddr()).getHostName().equals("localhost");
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return false;
        }
    }
}
