package sk.wladimiiir.intellihome.service.control;

import sk.wladimiiir.intellihome.model.unit.UnitControl;
import sk.wladimiiir.intellihome.service.processor.ProcessorService;
import sk.wladimiiir.intellihome.service.unit.UnitControlService;
import sk.wladimiiir.intellihome.service.unit.UnitService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
public class UnitControlServiceImpl implements UnitControlService {
    @Inject
    private volatile UnitService unitService;
    @Inject
    private volatile ProcessorService processorService;


    @Override
    public List<UnitControl> getUnitControls(String unitID) {
        final List<UnitControl> controls = new ArrayList<>();

        unitService.getUnit(unitID).ifPresent(unit -> controls.addAll(unit.getControls()));

        Collections.sort(controls, (o1, o2) -> o1.getType().ordinal() - o2.getType().ordinal());

        return controls;
    }

    public Optional<UnitControl> getUnitControl(String unitID, String unitControlID) {
        return getUnitControls(unitID).parallelStream()
                .filter(unitControl -> unitControlID.equals(unitControl.getID()))
                .findFirst();
    }
}
