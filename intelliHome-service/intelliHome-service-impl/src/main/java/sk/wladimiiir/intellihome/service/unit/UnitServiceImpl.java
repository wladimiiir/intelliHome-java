package sk.wladimiiir.intellihome.service.unit;

import sk.wladimiiir.intellihome.device.DeviceManager;
import sk.wladimiiir.intellihome.device.exception.DigitalDeviceError;
import sk.wladimiiir.intellihome.device.exception.PinException;
import sk.wladimiiir.intellihome.model.processor.Thermostat;
import sk.wladimiiir.intellihome.model.processor.ThreeWayValveController;
import sk.wladimiiir.intellihome.model.temperature.IncreasedTemperaturerRule;
import sk.wladimiiir.intellihome.model.temperature.SimpleTemperatureRule;
import sk.wladimiiir.intellihome.model.temperature.TemperatureRule;
import sk.wladimiiir.intellihome.model.unit.FloorHeatingUnit;
import sk.wladimiiir.intellihome.model.unit.RelayUnit;
import sk.wladimiiir.intellihome.model.unit.Unit;
import sk.wladimiiir.intellihome.model.unit.control.UnitStateUnitControl;
import sk.wladimiiir.intellihome.model.unit.control.TemperatureRuleControl;
import sk.wladimiiir.intellihome.model.unit.control.UnitStateRuleControl;
import sk.wladimiiir.intellihome.model.unit.control.UnitTimingControl;
import sk.wladimiiir.intellihome.service.thermometer.ThermometerDefinition;
import sk.wladimiiir.intellihome.service.thermometer.ThermometerService;
import sk.wladimiiir.intellihome.service.unit.control.UnitStateCalendarRuleService;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.*;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
public class UnitServiceImpl implements UnitService {
    private final List<Unit> units = new LinkedList<>();

    @Inject
    private volatile DeviceManager deviceManager;
    @Inject
    private volatile ThermometerService thermometerService;
    @Inject
    private volatile UnitStateCalendarRuleService unitStateCalendarRuleService;

    @PostConstruct
    private void init() throws DigitalDeviceError, PinException {
        units.add(createElectricHeaterUnit());
        units.add(createFloorHeatingUnit());
        registerRelayUnit(UnitDefinition.FIREPLACE_EXCHANGER_PUMP, "P8_8");
    }

    private void registerRelayUnit(UnitDefinition unitDefinition, String pinKey) throws PinException, DigitalDeviceError {
        final RelayUnit relayUnit = createRelayUnit(unitDefinition, pinKey);
        relayUnit.registerControl(createUnitStateUnitControl(relayUnit));
        units.add(relayUnit);
    }

    private RelayUnit createRelayUnit(UnitDefinition unitDefinition, String pinKey) throws DigitalDeviceError, PinException {
        return new RelayUnit(unitDefinition.getID(), unitDefinition.getName(), deviceManager.getDigitalOutputPinDevice(deviceManager.getPinByKey(pinKey)));
    }

    private Unit createElectricHeaterUnit() throws DigitalDeviceError, PinException {
        final RelayUnit electricHeater = createRelayUnit(UnitDefinition.ELECTRIC_HEATER, "P8_16");
        electricHeater.registerControl(createUnitStateUnitControl(electricHeater));
        electricHeater.registerControl(new UnitTimingControl(UUID.randomUUID().toString(), electricHeater));
        return electricHeater;
    }

    private FloorHeatingUnit createFloorHeatingUnit() throws DigitalDeviceError, PinException {
        final RelayUnit pumpUnit = createRelayUnit(UnitDefinition.FLOOR_HEATING, "P8_10");
        final TemperatureRuleControl floorHeatingTemperatureRule = getFloorHeatingTemperatureRule();
        final FloorHeatingUnit unit = new FloorHeatingUnit(
                UnitDefinition.FLOOR_HEATING.getID(),
                UnitDefinition.FLOOR_HEATING.getName(),
                pumpUnit,
                this.<ThreeWayValveController>createTreeWayValveController(floorHeatingTemperatureRule),
                createElectricHeaterProcessor(floorHeatingTemperatureRule),
                this.<RelayUnit>getUnit(UnitDefinition.ELECTRIC_HEATER.getID()).get()
        );

        unit.registerControl(createUnitStateUnitControl(unit));
		unit.registerControl(floorHeatingTemperatureRule);
        unit.registerControl(new UnitTimingControl(UUID.randomUUID().toString(), unit));

        return unit;
    }

    private UnitStateUnitControl createUnitStateUnitControl(Unit unit) {
        return new UnitStateRuleControl(unit, createUnitStateRule(unit));
    }

    private PersistedUnitStateCalendarRule createUnitStateRule(Unit unit) {
        return new PersistedUnitStateCalendarRule(unit.getID(), Optional::empty, unitStateCalendarRuleService);
    }

    private Thermostat createElectricHeaterProcessor(TemperatureRule floorHeatingTemperatureRule) {
        return new Thermostat(
                thermometerService.getThermometer(ThermometerDefinition.TANK_MIDDLE.getID()).get(),
                new IncreasedTemperaturerRule(floorHeatingTemperatureRule, 2.0f),
                Thermostat.ControlType.HEATING,
                getUnit(UnitDefinition.ELECTRIC_HEATER.getID()).get()
        );
    }

    private ThreeWayValveController createTreeWayValveController(TemperatureRule floorHeatingTemperatureRule) throws DigitalDeviceError, PinException {
        final RelayUnit valveLower = createRelayUnit(UnitDefinition.THREE_WAY_VALVE_LOWER, "P8_14");
        final RelayUnit valveRaise = createRelayUnit(UnitDefinition.THREE_WAY_VALVE_RAISE, "P8_12");

        return new ThreeWayValveController(
                thermometerService.getThermometer(ThermometerDefinition.FLOOR_HEATING_IN.getID()).get(),
                floorHeatingTemperatureRule,
                valveLower,
                valveRaise
        );
    }

    private TemperatureRuleControl getFloorHeatingTemperatureRule() {
        return new TemperatureRuleControl(UUID.randomUUID().toString(), ThermometerDefinition.FLOOR_HEATING_IN.getID(), new SimpleTemperatureRule(36, 38));
    }

    @Override
    public <T extends Unit> Optional<T> getUnit(String id) {
        //noinspection unchecked
        return (Optional<T>) units.stream()
                .filter(unit -> unit.getID().equals(id))
                .findFirst();
    }

    @Override
    public List<Unit> getUnits() {
        return Collections.unmodifiableList(units);
    }
}
