package sk.wladimiiir.intellihome.service.thermometer;

import org.springframework.scheduling.annotation.Scheduled;
import sk.wladimiiir.intellihome.dao.ThermometerDataDAO;
import sk.wladimiiir.intellihome.model.db.ThermometerData;
import sk.wladimiiir.intellihome.model.db.query.MinMaxAverageEntity;
import sk.wladimiiir.intellihome.model.exception.ThermometerException;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
public class ThermometerDataServiceImpl implements ThermometerDataService {
	@Inject
	private volatile ThermometerService thermometerService;
	@Inject
	private volatile ThermometerDataDAO thermometerDataDAO;

	@Scheduled(cron = "0 * * * * *")
	private void storeThermometerData() {
		thermometerService.getThermometers().forEach(thermometer -> {
			try {
				final float temperature = thermometer.getTemperature();
				thermometerDataDAO.save(new ThermometerData(thermometer.getID(), LocalDateTime.now(), temperature));
			} catch (ThermometerException e) {
				//skip
			}
		});
	}

	@Override
	public List<ThermometerData> getThermometerData(String thermometerID, LocalDateTime fromTime, LocalDateTime toTime) {
		return thermometerDataDAO.getData(thermometerID, fromTime, toTime);
	}

	@Override
	public List<MinMaxAverageEntity> getHourlyGroupedData(String thermometerID, LocalDateTime fromTime, LocalDateTime toTime) {
		return thermometerDataDAO.getDataForDatepart(thermometerID, "HOUR", fromTime, toTime);
	}

	@Override
	public List<MinMaxAverageEntity> getDailyGroupedData(String thermometerID, LocalDateTime fromTime, LocalDateTime toTime) {
		return thermometerDataDAO.getDataForDatepart(thermometerID, "DAY", fromTime, toTime);
	}

	@Override
	public List<MinMaxAverageEntity> getMonthlyGroupedData(String thermometerID, LocalDateTime fromTime, LocalDateTime toTime) {
		return thermometerDataDAO.getDataForDatepart(thermometerID, "MONTH", fromTime, toTime);
	}

	@Override
	public List<MinMaxAverageEntity> getYearlyGroupedData(String thermometerID) {
		return thermometerDataDAO.getDataForDatepart(thermometerID, "YEAR", LocalDate.of(2014, 1, 1).atStartOfDay(), LocalDateTime.now());
	}

    @Override
    public void removeLastYearData() {
        thermometerDataDAO.removeDataBefore(Year.now().atDay(1).atStartOfDay());
    }

    @Override
	public void removeFutureData() {
		thermometerDataDAO.removeDataAfter(LocalDateTime.now());
	}
}
