package sk.wladimiiir.intellihome.service.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import sk.wladimiiir.intellihome.common.utils.TimerUtils;
import sk.wladimiiir.intellihome.model.exception.UnitException;
import sk.wladimiiir.intellihome.model.unit.Unit;
import sk.wladimiiir.intellihome.service.thermometer.ThermometerDataService;
import sk.wladimiiir.intellihome.service.unit.UnitDataService;
import sk.wladimiiir.intellihome.service.unit.UnitDefinition;
import sk.wladimiiir.intellihome.service.unit.UnitService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.time.Duration;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
public class StandbyScheduleService {
    private static final Logger LOGGER = LoggerFactory.getLogger(StandbyScheduleService.class);

    @Inject
    private volatile UnitService unitService;
	@Inject
	private volatile UnitDataService unitDataService;
	@Inject
	private volatile ThermometerDataService thermometerDataService;

    @Scheduled(cron = "0 0 0 1 * *") //once monthly
    private void runFireplaceExchanger() {
        unitService.getUnit(UnitDefinition.FIREPLACE_EXCHANGER_PUMP.getID()).ifPresent(unit -> {
            if (isRunning(unit)) {
                return;
            }

            final Unit.State state = unit.getState();
            try {
                LOGGER.info("Standby fireplace exchanger pump started.");
                unit.forceRun();
                TimerUtils.startTimeout(() -> {
                    try {
                        switch (state) {
                            case STOPPED:
                                unit.resume();
                                break;
                            case SUSPENDED:
                                unit.suspend();
                                break;
                        }
                        LOGGER.info("Standby fireplace exchanger pump stopped.");
                    } catch (UnitException e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                }, Duration.ofMinutes(2));
            } catch (UnitException e) {
                LOGGER.error(e.getMessage(), e);
            }
        });
    }

	@Scheduled(cron = "0 0 * * * *") //once hourly
	private void removeFutureData() {
		unitDataService.removeFutureData();
		thermometerDataService.removeFutureData();
	}

    private boolean isRunning(Unit unit) {
        return unit.getState() == Unit.State.FORCE_RUN || unit.getState() == Unit.State.STARTED;
    }
}
