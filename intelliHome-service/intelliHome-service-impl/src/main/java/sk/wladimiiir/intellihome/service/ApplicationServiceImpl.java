package sk.wladimiiir.intellihome.service;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Named
@Singleton
public class ApplicationServiceImpl implements ApplicationService {
    @Inject
    private volatile MyHomeService myHomeService;

    @Override
    public void onStarted() {
        myHomeService.start();
    }

    @Override
    public void onStopped() {
        myHomeService.stop();
    }
}
