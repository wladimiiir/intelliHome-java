package sk.wladimiiir.intellihome.service.notification;

import sk.wladimiiir.intellihome.system.SystemConfigurationService;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
public class NotificationServiceImpl implements NotificationService {
    @Inject
    private volatile SystemConfigurationService configurationService;

    @PostConstruct
    private void init() {
    }

    private Properties getProperties() {
        final Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", configurationService.getNotificationEmailSMTPHost());
        properties.put("mail.smtp.port", configurationService.getNotificationEmailSMTPPort());
        return properties;
    }

    @Override
    public void applicationStarted() {
        sendNotification("intelliHome started", "Hello, I have just started.");
    }

    @Override
    public void applicationStopped() {
        sendNotification("intelliHome stopped", "Hello, I have been stopped.");
    }

    private void sendNotification(String subject, String body) {
        if (!checkConfiguration()) {
            return;
        }

        final Session session = Session.getDefaultInstance(getProperties(), new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(configurationService.getNotificationEmailUsername(), configurationService.getNotificationEmailPassword());
            }
        });
        final Message message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress("intelliHome"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(configurationService.getNotificationRecipients()));

            message.setSubject(subject);
            message.setText(body);

            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private boolean checkConfiguration() {
        return configurationService.getNotificationEmailSMTPHost() != null
                && configurationService.getNotificationEmailSMTPPort() != null
                && configurationService.getNotificationEmailUsername() != null
                && configurationService.getNotificationEmailPassword() != null
                && configurationService.getNotificationRecipients() != null;
    }
}
