package sk.wladimiiir.intellihome.service.auth;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Vladimir Hrusovsky
 */
public interface AuthenticationService {
    boolean authenticate(String password);

    boolean isAuthenticated(HttpServletRequest request);
}
