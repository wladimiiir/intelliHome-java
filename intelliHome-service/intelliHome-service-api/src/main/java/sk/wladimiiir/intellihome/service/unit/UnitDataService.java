package sk.wladimiiir.intellihome.service.unit;

import sk.wladimiiir.intellihome.model.db.query.DurationEntity;
import sk.wladimiiir.intellihome.model.db.query.UnitDataEntity;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public interface UnitDataService {
    List<UnitDataEntity> getUnitData(String unitID, LocalDateTime fromTime, LocalDateTime toTime);

	List<DurationEntity> getHourlyGroupedData(String unitID, LocalDateTime fromTime, LocalDateTime toTime);

	List<DurationEntity> getDailyGroupedData(String unitID, LocalDateTime fromTime, LocalDateTime toTime);

	List<DurationEntity> getMonthlyGroupedData(String unitID, LocalDateTime fromTime, LocalDateTime toTime);

	List<DurationEntity> getYearlyGroupedData(String unitID);

    void removeLastYearData();

    void removeFutureData();

    void close();
}
