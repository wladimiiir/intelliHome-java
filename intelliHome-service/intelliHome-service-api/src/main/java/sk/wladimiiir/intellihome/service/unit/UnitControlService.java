package sk.wladimiiir.intellihome.service.unit;

import sk.wladimiiir.intellihome.model.unit.UnitControl;

import java.util.List;
import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
public interface UnitControlService {
    List<UnitControl> getUnitControls(String unitID);

    Optional<UnitControl> getUnitControl(String unitID, String unitControlID);
}
