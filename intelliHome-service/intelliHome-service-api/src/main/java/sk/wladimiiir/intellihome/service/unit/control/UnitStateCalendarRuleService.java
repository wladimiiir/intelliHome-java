package sk.wladimiiir.intellihome.service.unit.control;

import sk.wladimiiir.intellihome.model.unit.control.calendar.UnitStateCalendarRuleEntity;

import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public interface UnitStateCalendarRuleService {
    void save(String parentID, List<UnitStateCalendarRuleEntity> rules);

    List<UnitStateCalendarRuleEntity> getRules(String parentID);
}
