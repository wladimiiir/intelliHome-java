package sk.wladimiiir.intellihome.service.auth;

import sk.wladimiiir.intellihome.service.javax.inject.Session;

import javax.inject.Named;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Session
public class AuthSession {
    private boolean authenticated;

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }
}
