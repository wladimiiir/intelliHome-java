package sk.wladimiiir.intellihome.service.notification;

/**
 * @author Vladimir Hrusovsky
 */
public interface NotificationService {
    void applicationStarted();

    void applicationStopped();
}
