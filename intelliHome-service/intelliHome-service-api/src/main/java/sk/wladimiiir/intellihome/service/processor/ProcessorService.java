package sk.wladimiiir.intellihome.service.processor;

import sk.wladimiiir.intellihome.model.processor.Processor;

import java.util.Collection;
import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public interface ProcessorService {
    Collection<Processor> getProcessors();
}
