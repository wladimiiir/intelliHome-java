package sk.wladimiiir.intellihome.service.javax.inject;

import javax.inject.Scope;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Session scope, which is a <i>javax.inject</i> scope equivalent for a <i>spring session scope</i>.
 * 
 * @author Stanislav Dvorscak
 *
 */
@Scope
@Documented
@Retention(RUNTIME)
public @interface Session {
}
