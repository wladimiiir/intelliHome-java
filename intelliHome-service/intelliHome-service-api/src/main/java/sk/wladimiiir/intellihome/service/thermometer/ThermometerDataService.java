package sk.wladimiiir.intellihome.service.thermometer;

import sk.wladimiiir.intellihome.model.db.ThermometerData;
import sk.wladimiiir.intellihome.model.db.query.MinMaxAverageEntity;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public interface ThermometerDataService {
    List<ThermometerData> getThermometerData(String thermometerID, LocalDateTime fromTime, LocalDateTime toTime);

	List<MinMaxAverageEntity> getHourlyGroupedData(String thermometerID, LocalDateTime fromTime, LocalDateTime toTime);

	List<MinMaxAverageEntity> getDailyGroupedData(String thermometerID, LocalDateTime fromTime, LocalDateTime toTime);

	List<MinMaxAverageEntity> getMonthlyGroupedData(String thermometerID, LocalDateTime fromTime, LocalDateTime toTime);

	List<MinMaxAverageEntity> getYearlyGroupedData(String thermometerID);

    void removeLastYearData();

    void removeFutureData();
}
