package sk.wladimiiir.intellihome.service;

/**
 * Application related services.
 */
public interface ApplicationService {

    /**
     * Called by assembly, to notify that application was fully started.
     */
    void onStarted();

    void onStopped();
}
