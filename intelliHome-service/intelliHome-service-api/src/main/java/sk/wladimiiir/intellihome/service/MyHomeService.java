package sk.wladimiiir.intellihome.service;

/**
 * @author wladimiiir
 * @since 7/5/15
 */
public interface MyHomeService {
    void start();

    void stop();
}
