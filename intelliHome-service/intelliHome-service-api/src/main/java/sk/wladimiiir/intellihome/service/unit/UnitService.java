package sk.wladimiiir.intellihome.service.unit;

import sk.wladimiiir.intellihome.model.unit.Unit;

import java.util.List;
import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
public interface UnitService {
    <T extends Unit> Optional<T> getUnit(String id);

    List<Unit> getUnits();
}
