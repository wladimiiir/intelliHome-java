package sk.wladimiiir.intellihome.service.thermometer;

import sk.wladimiiir.intellihome.model.thermometer.Thermometer;

import java.util.List;
import java.util.Optional;

/**
 * @author Vladimir Hrusovsky
 */
public interface ThermometerService {
    List<Thermometer> getThermometers();

    Optional<Thermometer> getThermometer(String id);

    Optional<String> getThermometerName(String id);
}
