package sk.wladimiiir.intellihome.service.unit.control;

import sk.wladimiiir.intellihome.model.unit.control.calendar.TemperatureCalendarRuleEntity;

import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public interface TemperatureCalendarRuleService {
    void save(String parentID, List<TemperatureCalendarRuleEntity> rules);

    List<TemperatureCalendarRuleEntity> getRules(String parentID);
}
