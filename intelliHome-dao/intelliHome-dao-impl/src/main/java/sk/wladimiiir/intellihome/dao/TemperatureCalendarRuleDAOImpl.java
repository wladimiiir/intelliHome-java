package sk.wladimiiir.intellihome.dao;

import sk.wladimiiir.intellihome.model.db.TemperatureCalendarRuleEntityImpl;
import sk.wladimiiir.intellihome.model.unit.control.calendar.TemperatureCalendarRuleEntity;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
@Transactional
public class TemperatureCalendarRuleDAOImpl implements TemperatureCalendarRuleDAO {
    @PersistenceContext
    private volatile EntityManager entityManager;

    @Override
    public void save(String parentID, List<TemperatureCalendarRuleEntity> rules) {
        entityManager.createQuery("DELETE FROM TemperatureCalendarRuleEntityImpl e WHERE e.parentID = :parentID")
                .setParameter("parentID", parentID)
                .executeUpdate();

        rules.stream()
                .map(rule -> createDBEntity(rule, parentID))
                .forEach(entity -> entityManager.persist(entity));
    }

    private TemperatureCalendarRuleEntityImpl createDBEntity(TemperatureCalendarRuleEntity rule, String parentID) {
        if (rule instanceof TemperatureCalendarRuleEntityImpl) {
            ((TemperatureCalendarRuleEntityImpl) rule).setParentID(parentID);
            return (TemperatureCalendarRuleEntityImpl) rule;
        } else {
            final TemperatureCalendarRuleEntityImpl entity = new TemperatureCalendarRuleEntityImpl();
            entity.setParentID(parentID);
            entity.setStartTime(rule.getStartTime());
            entity.setEndTime(rule.getEndTime());
            entity.setRecurrenceRule(rule.getRecurrenceRule());
            entity.setMinTemperature(rule.getMinTemperature());
            entity.setMaxTemperature(rule.getMaxTemperature());
            return entity;
        }
    }

    @Override
    public List<TemperatureCalendarRuleEntity> getRules(String parentID) {
        //noinspection unchecked
        return entityManager.createQuery("SELECT e FROM TemperatureCalendarRuleEntityImpl e WHERE e.parentID = :parentID")
                .setParameter("parentID", parentID)
                .getResultList();
    }
}
