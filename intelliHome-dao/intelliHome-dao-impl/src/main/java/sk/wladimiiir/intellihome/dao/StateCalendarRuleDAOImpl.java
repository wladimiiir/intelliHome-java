package sk.wladimiiir.intellihome.dao;

import sk.wladimiiir.intellihome.model.db.UnitStateCalendarRuleEntityImpl;
import sk.wladimiiir.intellihome.model.unit.control.calendar.UnitStateCalendarRuleEntity;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
@Transactional
public class StateCalendarRuleDAOImpl implements StateCalendarRuleDAO {
    @PersistenceContext
    private volatile EntityManager entityManager;

    @Override
    public void save(String parentID, List<UnitStateCalendarRuleEntity> rules) {
        entityManager.createQuery("DELETE FROM UnitStateCalendarRuleEntityImpl e WHERE e.parentID = :parentID")
                .setParameter("parentID", parentID)
                .executeUpdate();

        rules.stream()
                .map(rule -> createDBEntity(rule, parentID))
                .forEach(entity -> entityManager.persist(entity));
    }

    private UnitStateCalendarRuleEntityImpl createDBEntity(UnitStateCalendarRuleEntity rule, String parentID) {
        if (rule instanceof UnitStateCalendarRuleEntityImpl) {
            ((UnitStateCalendarRuleEntityImpl) rule).setParentID(parentID);
            return (UnitStateCalendarRuleEntityImpl) rule;
        } else {
            final UnitStateCalendarRuleEntityImpl entity = new UnitStateCalendarRuleEntityImpl();
            entity.setParentID(parentID);
            entity.setStartTime(rule.getStartTime());
            entity.setEndTime(rule.getEndTime());
            entity.setRecurrenceRule(rule.getRecurrenceRule());
            entity.setRunning(rule.isRunning());
            return entity;
        }
    }

    @Override
    public List<UnitStateCalendarRuleEntity> getRules(String parentID) {
        //noinspection unchecked
        return entityManager.createQuery("SELECT e FROM UnitStateCalendarRuleEntityImpl e WHERE e.parentID = :parentID")
                .setParameter("parentID", parentID)
                .getResultList();
    }
}
