package sk.wladimiiir.intellihome.dao;

import sk.wladimiiir.intellihome.model.db.ThermometerData;
import sk.wladimiiir.intellihome.model.db.query.MinMaxAverageEntity;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
@Transactional
public class ThermometerDataDAOImpl implements ThermometerDataDAO {
    @PersistenceContext
    private volatile EntityManager entityManager;

    @Override
    public void save(ThermometerData thermometerData) {
        entityManager.persist(thermometerData);
    }

    @Override
    public List<ThermometerData> getData(String thermometerID, LocalDateTime fromTime, LocalDateTime toTime) {
        return entityManager.createQuery("SELECT td FROM ThermometerData as td " +
                "WHERE td.thermometerID = :thermometerID AND td.dateTime >= :fromTime AND td.dateTime <= :toTime", ThermometerData.class)
                .setParameter("thermometerID", thermometerID)
                .setParameter("fromTime", fromTime)
                .setParameter("toTime", toTime)
                .getResultList();
    }

    @Override
    public List<MinMaxAverageEntity> getDataForDatepart(String thermometerID, String datepart, LocalDateTime fromTime, LocalDateTime toTime) {
        @SuppressWarnings("unchecked")
        final List<Object[]> resultList = entityManager.createNativeQuery("SELECT EXTRACT(" + datepart + " FROM td.dateTime), MIN(td.temperature), MAX(td.temperature), AVG(td.temperature) " +
                "FROM thermometer_data as td " +
                "WHERE td.thermometerID = :thermometerID AND td.dateTime >= :fromTime AND td.dateTime <= :toTime " +
                "GROUP BY EXTRACT(" + datepart + " FROM td.dateTime)")
                .setParameter("thermometerID", thermometerID)
                .setParameter("fromTime", Timestamp.valueOf(fromTime))
                .setParameter("toTime", Timestamp.valueOf(toTime))
                .getResultList();
        return resultList.parallelStream()
                .map(data -> new MinMaxAverageEntity(
                        (Integer) data[0],
                        data[1] instanceof Float ? (Float) data[1] : ((Double) data[1]).floatValue(),
                        data[2] instanceof Float ? (Float) data[2] : ((Double) data[2]).floatValue(),
                        data[3] instanceof Float ? (Float) data[3] : ((Double) data[3]).floatValue())
                )
                .collect(Collectors.toList());
    }

    @Override
    public void removeDataAfter(LocalDateTime after) {
        entityManager.createQuery("DELETE FROM ThermometerData as td " +
                "WHERE td.dateTime > :after")
                .setParameter("after", after)
                .executeUpdate();
    }

    @Override
    public void removeDataBefore(LocalDateTime before) {
        entityManager.createQuery("DELETE FROM ThermometerData as td " +
                "WHERE td.dateTime < :before")
                .setParameter("before", before)
                .executeUpdate();
    }
}
