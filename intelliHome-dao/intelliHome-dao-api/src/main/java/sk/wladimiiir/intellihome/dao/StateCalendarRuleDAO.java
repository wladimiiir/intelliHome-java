package sk.wladimiiir.intellihome.dao;

import sk.wladimiiir.intellihome.model.unit.control.calendar.UnitStateCalendarRuleEntity;

import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public interface StateCalendarRuleDAO {
    void save(String parentID, List<UnitStateCalendarRuleEntity> rules);

    List<UnitStateCalendarRuleEntity> getRules(String parentID);
}
