package sk.wladimiiir.intellihome.dao;

import sk.wladimiiir.intellihome.model.unit.control.calendar.TemperatureCalendarRuleEntity;

import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public interface TemperatureCalendarRuleDAO {
    void save(String parentID, List<TemperatureCalendarRuleEntity> rules);

    List<TemperatureCalendarRuleEntity> getRules(String parentID);
}
