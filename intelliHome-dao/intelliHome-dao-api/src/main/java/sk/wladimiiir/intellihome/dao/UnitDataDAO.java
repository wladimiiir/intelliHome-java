package sk.wladimiiir.intellihome.dao;

import sk.wladimiiir.intellihome.model.db.UnitData;
import sk.wladimiiir.intellihome.model.db.query.DurationEntity;
import sk.wladimiiir.intellihome.model.db.query.UnitDataEntity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * @author Vladimir Hrusovsky
 */
public interface UnitDataDAO {
	void save(UnitData uniData);

	List<UnitDataEntity> getUnitData(String unitID, LocalDateTime fromTime, LocalDateTime toTime);

	List<DurationEntity> getDurationData(String unitID, LocalDateTime fromTime, LocalDateTime toTime, Function<LocalDateTime, LocalDateTime> stepFunction);

	Optional<UnitData> getLastUnitData(String unitID);

	void removeDataAfter(LocalDateTime after);

    void removeDataBefore(LocalDateTime before);
}
