package sk.wladimiiir.intellihome.dao;

import sk.wladimiiir.intellihome.model.db.ThermometerData;
import sk.wladimiiir.intellihome.model.db.query.MinMaxAverageEntity;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public interface ThermometerDataDAO {
    void save(ThermometerData thermometerData);

    List<ThermometerData> getData(String thermometerID, LocalDateTime fromTime, LocalDateTime toTime);

	List<MinMaxAverageEntity> getDataForDatepart(String thermometerID, String datepart, LocalDateTime fromTime, LocalDateTime toTime);

    void removeDataAfter(LocalDateTime after);

    void removeDataBefore(LocalDateTime before);
}
