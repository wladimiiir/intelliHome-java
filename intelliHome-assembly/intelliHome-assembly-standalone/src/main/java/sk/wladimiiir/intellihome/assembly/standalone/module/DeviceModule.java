package sk.wladimiiir.intellihome.assembly.standalone.module;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Jsr330ScopeMetadataResolver;

@Configuration
@ComponentScan(basePackages = "sk.wladimiiir.intellihome.device", scopeResolver = Jsr330ScopeMetadataResolver.class)
public class DeviceModule {
}
