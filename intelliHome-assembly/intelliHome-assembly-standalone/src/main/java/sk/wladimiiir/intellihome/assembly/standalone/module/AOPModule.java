package sk.wladimiiir.intellihome.assembly.standalone.module;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Jsr330ScopeMetadataResolver;

/**
 * @author Vladimir Hrusovsky
 */
@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages = "sk.wladimiiir.intellihome.assembly.standalone.aop", scopeResolver = Jsr330ScopeMetadataResolver.class)
public class AOPModule {
}
