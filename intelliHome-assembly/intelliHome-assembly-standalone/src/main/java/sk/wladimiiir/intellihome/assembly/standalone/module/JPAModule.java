package sk.wladimiiir.intellihome.assembly.standalone.module;

import org.apache.commons.dbcp2.managed.BasicManagedDataSource;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.engine.transaction.jta.platform.internal.JBossStandAloneJtaPlatform;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import sk.wladimiiir.intellihome.system.SystemConfigurationService;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import javax.transaction.TransactionManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Starts JPA related staff.
 */
@Configuration
public class JPAModule {

    /**
     * Injected {@link SystemConfigurationService} dependency.
     */
    @Inject
    private volatile SystemConfigurationService systemConfigurationService;

    /**
     * Injected {@link TransactionManager} dependency.
     */
    @Inject
    private volatile TransactionManager transactionManager;

    /**
     * Constructor.
     */
    public JPAModule() {
    }

    /**
     * @return DB Data source.
     */
    @Bean
    public DataSource dataSource() {
        BasicManagedDataSource result = new BasicManagedDataSource();
        result.setDriverClassName(systemConfigurationService.getJDBCDriver());
        result.setUrl(systemConfigurationService.getJDBCUrl());
        result.setUsername(systemConfigurationService.getJDBCUsername());
        result.setPassword(systemConfigurationService.getJDBCPassword());
        result.setTransactionManager(transactionManager);
        result.setDefaultAutoCommit(false);
        return result;
    }

    /**
     * @param dataSource injected {@link DataSource} dependency
     * @return JPA {@link EntityManager} factory.
     */
    @Bean
    public FactoryBean<EntityManagerFactory> entityManagerFactory(DataSource dataSource) throws SQLException {
        Properties jpaProperties = new Properties();

        // JTA related
        jpaProperties.setProperty(AvailableSettings.CURRENT_SESSION_CONTEXT_CLASS, "jta");
        jpaProperties.setProperty(AvailableSettings.JTA_PLATFORM, JBossStandAloneJtaPlatform.class.getCanonicalName());
        jpaProperties.setProperty(AvailableSettings.GLOBALLY_QUOTED_IDENTIFIERS, "false");
        jpaProperties.setProperty(AvailableSettings.RELEASE_CONNECTIONS, "after_statement");

        // JPA Vendor related
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setShowSql(false);
        vendorAdapter.setGenerateDdl(true);
        vendorAdapter.setDatabasePlatform(systemConfigurationService.getHibernateDialect());

        LocalContainerEntityManagerFactoryBean result = new LocalContainerEntityManagerFactoryBean();
        result.setJtaDataSource(dataSource);
        result.setJpaVendorAdapter(vendorAdapter);
        result.setPackagesToScan("sk.wladimiiir.intellihome.model.db");
        result.setJpaProperties(jpaProperties);
        return result;
    }
}
