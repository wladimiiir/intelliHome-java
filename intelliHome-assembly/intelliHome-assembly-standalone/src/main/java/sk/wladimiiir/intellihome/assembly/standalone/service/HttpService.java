package sk.wladimiiir.intellihome.assembly.standalone.service;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.websocket.jsr356.server.ServerContainer;
import org.eclipse.jetty.websocket.jsr356.server.deploy.WebSocketServerContainerInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.context.WebApplicationContext;
import sk.wladimiiir.intellihome.system.SystemConfigurationService;
import sk.wladimiiir.intellihome.system.exception.FatalIntelliHomeException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.ServletException;
import javax.websocket.DeploymentException;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.List;

/**
 * Setups HTTP service.S
 */
@Configuration
public class HttpService {

    /**
     * Logger for this module.
     */
    private final Logger logger = LoggerFactory.getLogger(HttpService.class);

    /**
     * Injected {@link SystemConfigurationService} dependency.
     */
    @Inject
    private volatile SystemConfigurationService systemConfigurationService;

    /**
     * Injected {@link Handler}-s dependencies, which will be registered.
     */
    @Inject
    private volatile Provider<List<Handler>> handlers;

    /**
     * Injected {@link FilterProvider}-s dependencies.
     */
    @Inject
    private volatile Provider<List<FilterProvider>> filterProviders;

    /**
     * Injected {@link ApplicationContext} dependency.
     */
    @Inject
    private volatile ApplicationContext applicationContext;

    /**
     * Jetty server.
     */
    private Server server;

    /**
     * Constructor.
     */
    public HttpService() {
    }

    /**
     * Starts {@link HttpService}.
     */
    @PostConstruct
    public void init() {
        server = server();
        start();
    }

    /**
     * Stops {@link HttpService}.
     *
     * @see #stop()
     */
    @PreDestroy
    public void destroy() {
        stop();
    }

    /**
     * @return Jetty Server.
     */
    public Server server() {
        final Server server = new Server();

        server.addConnector(connector(server));
        server.setHandler(handler(server));
        initWebSocketSupport(server);

        return server;
    }

    private void initWebSocketSupport(Server server) {
        try {
            final ServerContainer serverContainer = WebSocketServerContainerInitializer.configureContext(server.getChildHandlerByClass(ServletContextHandler.class));
            applicationContext.getBeansWithAnnotation(ServerEndpoint.class).forEach((name, endpoint) -> {
                try {
                    serverContainer.addEndpoint(endpoint.getClass());
                } catch (DeploymentException e) {
                    throw new FatalIntelliHomeException("Cannot init WebSocket support", e);
                }
            });
        } catch (ServletException e) {
            throw new FatalIntelliHomeException("Cannot init WebSocket support", e);
        }
    }

    /**
     * @return main handler
     * @param server
     */
    public Handler handler(Server server) {
        HandlerList result = new HandlerList();
        handlers.get().forEach(result::addHandler);
        return result;
    }

    /**
     * @return Static META-INF/resources.
     */
    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public Handler resources() {
        ContextHandler result = new ContextHandler(systemConfigurationService.getServiceHttpContextPath());
        HandlerList resourceHandlers = new HandlerList();
        Enumeration<URL> resources;
        try {
            resources = ClassLoader.getSystemResources("META-INF/resources");
        } catch (IOException e) {
            throw new FatalIntelliHomeException("Can not find META-INF/resources:", e);
        }
        Collections.list(resources).stream().map(resource -> {
            ResourceHandler resourceHandler = new ResourceHandler();
            resourceHandler.setResourceBase(resource.toExternalForm());
            return resourceHandler;
        }).forEach(resourceHandlers::addHandler);
        result.setHandler(resourceHandlers);
        return result;
    }

    /**
     * @return Servlet handler.
     */
    @Bean
    public Handler servlet() {
        ServletContextHandler result = new ServletContextHandler(ServletContextHandler.SESSIONS);
        result.getServletContext().setAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE, applicationContext);
        result.setContextPath(systemConfigurationService.getServiceHttpContextPath());

        filterProviders.get()
                .forEach(provider -> result.addFilter(provider.getHolder(), provider.getPathSpecs(), provider.getDispatches()));

        return result;
    }

    /**
     * Server connector.
     *
     * @param server over which server
     * @return connector
     */
    private Connector connector(Server server) {
        ServerConnector result = new ServerConnector(server);
        result.setPort(systemConfigurationService.getServiceHttpPort());
        return result;
    }

    /**
     * Starts HTTP listening.
     */
    public void start() {
        try {
            logger.info("Starting HTTP service: ...");
            server.start();
            logger.info("Starting HTTP service: done.");
        } catch (Exception e) {
            throw new FatalIntelliHomeException("Can not start the service:", e);
        }
    }

    /**
     * Stops HTTP listening.
     */
    public void stop() {
        try {
            logger.info("Stopping HTTP service: ...");
            server.stop();
            logger.info("Stopping HTTP service: done.");
        } catch (Exception e) {
            logger.error("The sevice was not shutdown properly:", e);
        }
    }

    /**
     * Contract for {@link Filter} providers.
     */
    public interface FilterProvider {

        /**
         * @return filter configuration
         */
        FilterHolder getHolder();

        /**
         * @return Filter path.
         */
        String getPathSpecs();

        /**
         * @return dispatched types
         */
        EnumSet<DispatcherType> getDispatches();

    }

}
