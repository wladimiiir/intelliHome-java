package sk.wladimiiir.intellihome.assembly.standalone.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import sk.wladimiiir.intellihome.service.auth.AuthenticationService;
import sk.wladimiiir.intellihome.service.auth.NoAuthorization;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 * @author Vladimir Hrusovsky
 */
@Aspect
@Named
@Singleton
public class AuthorizationAspect {
    @Inject
    private volatile AuthenticationService authenticationService;

    @Pointcut("execution(public * sk.wladimiiir.intellihome.view.rest.*Resource.*(..))")
    private void publicRestCall() {
        //pointcut
    }

    @Before("publicRestCall() && @annotation(authorization)")
    public void ignoreAuthorizationCall(NoAuthorization authorization) {
        //no authorization required
    }

    @Before("publicRestCall() && !@annotation(sk.wladimiiir.intellihome.service.auth.NoAuthorization)")
    public void authorizeCall() {
        if (!authenticationService.isAuthenticated(getRequest())) {
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }
    }

    public HttpServletRequest getRequest() {
        final RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
        return requestAttributes instanceof ServletRequestAttributes
                ? ((ServletRequestAttributes) requestAttributes).getRequest()
                : null;
    }
}
