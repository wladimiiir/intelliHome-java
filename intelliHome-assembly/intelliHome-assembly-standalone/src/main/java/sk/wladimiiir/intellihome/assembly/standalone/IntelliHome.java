package sk.wladimiiir.intellihome.assembly.standalone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import sk.wladimiiir.intellihome.assembly.standalone.module.*;
import sk.wladimiiir.intellihome.assembly.standalone.service.HttpService;
import sk.wladimiiir.intellihome.service.ApplicationService;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Main launcher of the application.
 */
public class IntelliHome {

    /**
     * Logger for this class.
     */
    private final Logger logger = LoggerFactory.getLogger(IntelliHome.class);

    /**
     * Application context of this application.
     */
    private final AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();

    /**
     * Shutdown hook of the application.
     */
    private final Thread shutdownHook = new Thread(this::stop, "System Shutdown");

    /**
     * Waits until application will start.
     */
    private final CountDownLatch waitForStart = new CountDownLatch(1);

    /**
     * Flag - if it is true application is going down.
     */
    private final AtomicBoolean stopping = new AtomicBoolean();

    /**
     * Starts the application.
     */
    public void start() {
        Runtime.getRuntime().addShutdownHook(shutdownHook);

        logger.info("Starting application: ...");
        applicationContext.addApplicationListener(event -> {
            if (event instanceof ContextStartedEvent) {
                applicationContext.getBean(ApplicationService.class).onStarted();
                logger.info("Starting application: done.");
                waitForStart.countDown();
            }
        });
        applicationContext.register(SystemModule.class);
        applicationContext.register(JTAModule.class);
        applicationContext.register(JPAModule.class);
        applicationContext.register(DAOModule.class);
        applicationContext.register(ServiceModule.class);
        applicationContext.register(JAXRSModule.class);
        applicationContext.register(DeviceModule.class);
        applicationContext.register(AOPModule.class);
        applicationContext.register(HttpService.class);
        try {
            applicationContext.refresh();
            applicationContext.start();
        } catch (BeanCreationException e) {
            waitForStart.countDown();
            stop();
        }
        try {
            waitForStart.await();
        } catch (InterruptedException e) {
            if (!stopping.get()) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Stops the application.
     */
    public void stop() {
        // only one stop will be performed, all others will be ignored
        if (stopping.compareAndSet(false, true)) {
            if (!shutdownHook.equals(Thread.currentThread())) {
                Runtime.getRuntime().removeShutdownHook(shutdownHook);
            }
            logger.info("Stopping application: ...");
            applicationContext.getBean(ApplicationService.class).onStopped();
            applicationContext.close();
            logger.info("Stopping application: done.");
            logger.info("Good Bye!");
        }
    }

    /**
     * main()
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        IntelliHome intelliHome = new IntelliHome();
        intelliHome.start();
    }

}
