package sk.wladimiiir.intellihome.assembly.standalone.module;

import org.eclipse.jetty.servlet.FilterHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.RequestContextFilter;
import sk.wladimiiir.intellihome.assembly.standalone.service.HttpService;
import sk.wladimiiir.intellihome.view.rest.SystemResource;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.DispatcherType;
import javax.ws.rs.Path;
import javax.ws.rs.ext.Provider;
import java.util.EnumSet;

/**
 * Loads REST/JAX-RS module.
 */
@Configuration
@ComponentScan(basePackages = "sk.wladimiiir.intellihome.view.rest", scopeResolver = Jsr330ScopeMetadataResolver.class)
public class JAXRSModule {

    /**
     * Relative URI to REST filter.
     */
    private static final String REST_FILTER_URI = "/rest";

    /**
     * Constructor.
     */
    public JAXRSModule() {
    }

    /**
     * @return {@link HttpService.FilterProvider} for a JAX-RS.
     */
    @Bean
    public HttpService.FilterProvider jaxRSFilter() {
        return new HttpService.FilterProvider() {

            @Override
            public FilterHolder getHolder() {
                FilterHolder filterHolder = new FilterHolder(ServletContainer.class);
                filterHolder.setInitParameter("javax.ws.rs.Application", JAXRSApplication.class.getName());
                filterHolder.setInitParameter("jersey.config.servlet.filter.contextPath", REST_FILTER_URI);
                return filterHolder;
            }

            @Override
            public String getPathSpecs() {
                return REST_FILTER_URI + "/*";
            }

            @Override
            public EnumSet<DispatcherType> getDispatches() {
                return EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST);
            }
        };
    }

    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public HttpService.FilterProvider requestFilter() {
        return new HttpService.FilterProvider() {
            @Override
            public FilterHolder getHolder() {
                return new FilterHolder(RequestContextFilter.class);
            }

            @Override
            public String getPathSpecs() {
                return REST_FILTER_URI + "/*";
            }

            @Override
            public EnumSet<DispatcherType> getDispatches() {
                return EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST);
            }
        };
    }

    /**
     * JAX-RS application.
     */
    @Named
    @Singleton
    public static class JAXRSApplication extends ResourceConfig {

        /**
         * Injected {@link SystemResource} dependency.
         */
        @Inject
        private volatile ApplicationContext applicationContext;

        /**
         * Constructor.
         */
        public JAXRSApplication() {
        }

        /**
         * Registers JAX-RS providers and resources.
         */
        @PostConstruct
        public void init() {
            applicationContext.getBeansWithAnnotation(Provider.class).forEach((name, provider) -> register(provider));
            applicationContext.getBeansWithAnnotation(Path.class).forEach((name, resource) -> register(resource));
        }

    }

}
