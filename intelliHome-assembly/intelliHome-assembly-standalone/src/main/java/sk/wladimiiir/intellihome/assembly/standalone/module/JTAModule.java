package sk.wladimiiir.intellihome.assembly.standalone.module;

import com.arjuna.ats.arjuna.common.arjPropertyManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.jta.JtaTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;
import sk.wladimiiir.intellihome.system.SystemConfigurationService;

import javax.inject.Inject;
import javax.transaction.TransactionManager;
import java.nio.file.Paths;

/**
 * JTA module.
 */
@Configuration
@EnableTransactionManagement
public class JTAModule {

    /**
     * Injected {@link SystemConfigurationService} dependency.
     */
    @Inject
    private volatile SystemConfigurationService systemConfigurationService;

    /**
     * Constructor.
     */
    public JTAModule() {
    }

    /**
     * @return Creates JTA transaction manager.
     */
    @Bean
    public TransactionManager jta() {
        String objectStoreDir = Paths.get(systemConfigurationService.getVarDir() + "/jta", "object-store").toString();
        System.setProperty("com.arjuna.ats.arjuna.objectstore.objectStoreDir", objectStoreDir);
        arjPropertyManager.getObjectStoreEnvironmentBean().setObjectStoreDir(objectStoreDir);
        return com.arjuna.ats.jta.TransactionManager.transactionManager();
    }

    /**
     * @param transactionManager JTA transaction manager
     * @return Spring transaction manager.
     */
    @Bean
    public JtaTransactionManager springTransactionManager(TransactionManager transactionManager) {
        return new JtaTransactionManager(transactionManager);
    }

    /**
     * @return {@link TransactionTemplate} over {@link #springTransactionManager)}.
     */
    @Bean
    public TransactionTemplate transactionTemplate(JtaTransactionManager jtaTransactionManager) {
        TransactionTemplate result = new TransactionTemplate();
        result.setTimeout(600);
        result.setTransactionManager(jtaTransactionManager);
        return result;
    }

}
