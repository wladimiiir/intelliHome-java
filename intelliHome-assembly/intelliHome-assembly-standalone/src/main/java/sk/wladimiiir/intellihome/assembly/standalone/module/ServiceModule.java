package sk.wladimiiir.intellihome.assembly.standalone.module;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import sk.wladimiiir.intellihome.assembly.standalone.inject.Jsr330ScopeMetadataResolver;

import javax.annotation.PreDestroy;
import javax.inject.Named;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Setups Service layer.
 */
@Configuration
@EnableScheduling
@ComponentScan(basePackages = "sk.wladimiiir.intellihome.service", scopeResolver = Jsr330ScopeMetadataResolver.class)
public class ServiceModule {

    /**
     * Logger for this class.
     */
    private final Logger logger = LoggerFactory.getLogger(ServiceModule.class);

    /**
     * Reference to {@link #unlimitedExecutorService()}.
     */
    private volatile ExecutorService unlimitedExecutorService;

    /**
     * Constructor.
     */
    public ServiceModule() {
    }

    /**
     * Destroys this module.
     */
    @PreDestroy
    public void destroy() {
        if (unlimitedExecutorService != null) {
            unlimitedExecutorService.shutdown();
            try {
                unlimitedExecutorService.awaitTermination(10, TimeUnit.MINUTES);
            } catch (InterruptedException e) {
                logger.error("Unlimited executor service was not shutdown properly!");
            }
        }
    }

    /**
     * @return unlimited {@link ExecutorService}
     */
    @Bean
    @Named("unlimited")
    public ExecutorService unlimitedExecutorService() {
        return unlimitedExecutorService = Executors.newCachedThreadPool();
    }

}
