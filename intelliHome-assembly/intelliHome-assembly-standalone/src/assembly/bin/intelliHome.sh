#!/bin/sh
if [[ $1 == "sleep" ]]
  then
    sleep 1m
fi

INTELLIHOME_USR="`dirname $0`/.."
cd $INTELLIHOME_USR
CLASSPATH=$(echo "./etc:`ls ./lib/*.jar | tr "\\n" ":"``ls ./lib/intelliHome-assembly-standalone-*.jar`")
$JAVA_HOME/bin/java -cp $CLASSPATH sk.wladimiiir.intellihome.assembly.standalone.IntelliHome
