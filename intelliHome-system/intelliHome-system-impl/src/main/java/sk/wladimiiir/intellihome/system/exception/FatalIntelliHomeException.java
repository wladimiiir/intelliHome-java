package sk.wladimiiir.intellihome.system.exception;

/**
 * @author wladimiiir
 * @since 7/4/15
 */
public class FatalIntelliHomeException extends RuntimeException {
    public FatalIntelliHomeException(String message) {
        super(message);
    }

    public FatalIntelliHomeException(String message, Throwable cause) {
        super(message, cause);
    }
}
