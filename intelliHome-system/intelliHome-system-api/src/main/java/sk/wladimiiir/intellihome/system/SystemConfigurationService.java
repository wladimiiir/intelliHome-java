package sk.wladimiiir.intellihome.system;

/**
 * Provides access to system configuration.
 */
public interface SystemConfigurationService {

    String PROPERTIES_FILE = "etc/intelliHome.properties";
    String VAR_DIR = "var.dir";
    String JDBC_DRIVER = "jdbc.driver";
    String JDBC_URL = "jdbc.url";
    String JDBC_USERNAME = "jdbc.username";
    String JDBC_PASSWORD = "jdbc.password";
    String HIBERNATE_DIALECT = "hibernate.dialect";
    String SERVICE_HTTP_PORT = "service.http.port";
    String SERVICE_HTTP_CONTEXT_PATH = "service.http.contextPath";
    String URL_CONNECTION_TIMEOUT = "url.connection.timeout";
    String URL_CONNECTION_READ_TIMEOUT = "url.connection.read.timeout";
    String NOTIFICATION_EMAIL_SMTP_HOST = "notification.email.smtp.host";
    String NOTIFICATION_EMAIL_SMTP_PORT = "notification.email.smtp.port";
    String NOTIFICATION_EMAIL_USERNAME = "notification.email.username";
    String NOTIFICATION_EMAIL_PASSWORD = "notification.email.password";
    /**
     * Comma separated list of email, which will be notified by the system.
     */
    String NOTIFICATION_RECIPIENTS = "notification.email.recipients";

    String getVarDir();

    String getJDBCDriver();

    String getJDBCUrl();

    String getJDBCUsername();

    String getJDBCPassword();

    String getHibernateDialect();

    int getServiceHttpPort();

    String getServiceHttpContextPath();

    int getUrlConnectionTimeout();

    int getUrlConnectionReadTimeout();

    String getNotificationEmailSMTPHost();

    String getNotificationEmailSMTPPort();

    String getNotificationEmailUsername();

    String getNotificationEmailPassword();

    String getNotificationRecipients();
}
