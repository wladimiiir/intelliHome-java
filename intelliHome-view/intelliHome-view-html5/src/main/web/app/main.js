var React = require('react');
var ReactDOM = require('react-dom');
var Router = require('react-router').Router;
var Route = require('react-router').Route;
var Redirect = require('react-router').Redirect;
var App = require('./App.jsx');

var MainPage = require('./pages/MainPage.jsx');
var Overview = require('./components/main/Overview.jsx');
var Statistics = require('./components/main/Statistics.jsx');
var Controls = require('./components/main/Controls.jsx');

var SchedulerPage = require('./pages/SchedulerPage.jsx');
var StateScheduler = require('./components/scheduler/UnitStateScheduler.jsx');
var TemperatureScheduler = require('./components/scheduler/TemperatureScheduler.jsx');

ReactDOM.render(
    <Router>
        <Redirect from="/" to="main/overview"/>
        <Route path="/" component={App}>
            <Route path="main" component={MainPage}>
                <Route path="overview" component={Overview}/>
                <Route path="stats" component={Statistics}/>
                <Route path="controls/:unitID" component={Controls}/>
            </Route>
            <Route path="scheduler" component={SchedulerPage}>
                <Route path="state/:unitID/:unitControlID" component={StateScheduler}/>
                <Route path="temperature/:unitID/:unitControlID" component={TemperatureScheduler}/>
            </Route>
        </Route>
    </Router>,
    document.getElementById("root")
);