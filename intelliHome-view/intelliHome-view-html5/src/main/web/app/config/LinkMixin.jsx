var React = require('react');
var History = require('react-router').History;

var LinkMixin = React.createMixin({
    mixins: [History],

    goToLink: function (link) {
        this.history.pushState(null, link);
    },

    goToHome: function () {
        this.goToLink("/home");
    },

    goToMainPageNode: function (nodeID) {
        this.goToLink("/main/" + nodeID);
    },

    goToGettingStarted: function () {
        this.goToMainPageNode("getting_started");
    },

    goToBrowseAPI: function () {
        this.goToMainPageNode("browse_api");
    },

    goToSupport: function () {
        this.goToMainPageNode("support");
    },

    goToForum: function () {
        this.goToLink("/forum");
    },

    goToBlog: function () {
        this.goToLink("/blog");
    },

    goToMyAppsPageNode: function (nodeID) {
        this.goToLink("/my_apps/" + nodeID);
    },

    goToMyApps: function () {
        this.goToMyAppsPageNode("my_apps");
    },

    goToAdministrationPageNode: function (nodeID) {
        this.goToLink("/admin/" + nodeID);
    },

    goToAdministration: function () {
        this.goToAdministrationPageNode("connection");
    },

    goToUnauthorized: function () {
        this.goToLink("/");
    }
});
module.exports = LinkMixin;