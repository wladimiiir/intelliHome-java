var HistoryRange = {
    HOUR: "HOUR",
    DAY: "DAY",
    MONTH: "MONTH",
    YEAR: "YEAR",
    YEARS: "YEARS"
};
module.exports = HistoryRange;