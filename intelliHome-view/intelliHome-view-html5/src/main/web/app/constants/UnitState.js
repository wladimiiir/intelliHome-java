var UnitState = {
    STOPPED: "STOPPED",
    STARTED: "STARTED",
    SUSPENDED: "SUSPENDED",
    FORCE_RUN: "FORCE_RUN",

    ON: "ON",
    OFF: "OFF",
    AUTO: "AUTO"
};
module.exports = UnitState;