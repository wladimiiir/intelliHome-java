var React = require('react');

var Loading = React.createClass({
    getDefaultProps: function () {
        return {
            className: "loading",
            size: 30,
            color: null
        }
    },

    render: function () {
        var style = {
            fontSize: this.props.size
        };

        if(this.props.color) {
            style.color = this.props.color;
        }

        return (
            <div className={this.props.className}>
                <div className="icon" style={style}>
                    <i className="fa fa-spinner fa-pulse"></i>
                </div>
            </div>
        );
    }
});
module.exports = Loading;