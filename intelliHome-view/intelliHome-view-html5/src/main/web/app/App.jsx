var React = require('react');
var Reflux = require('reflux');
var Config = require('./config/Config');
var UnitStore = require('./store/UnitStore');
var SystemStore = require('./store/SystemStore');
var Link = require('react-router').Link;
var AuthorizedComponent = require('./authentication/AuthorizedComponent.jsx');
var moment = require('moment');
var NotificationSystem = require('react-notification-system');
var _ = require('lodash');

var ControlsMenu = React.createClass({
    mixins: [
        Reflux.connect(UnitStore, "unitData")
    ],

    getInitialState: function () {
        return {
            menuVisible: false
        };
    },

    toggleMenu: function () {
        this.setState({
            menuVisible: !this.state.menuVisible
        })
    },

    menuItemClicked: function () {
        this.setState({
            menuVisible: false
        });
    },

    getUnitItem: function (unit) {
        return <Link className="menuItem"
                     key={unit.id}
                     onClick={this.menuItemClicked}
                     to={"/main/controls/" + unit.id}>{unit.name}</Link>
    },

    getMenu: function () {
        var menuItems = null;

        if (this.state.unitData) {
            menuItems = this.state.unitData.map(this.getUnitItem);
        }

        return (
            <div className={this.state.menuVisible ? "menu" : "menu hidden"}>
                {menuItems}
            </div>
        );
    },

    render: function () {
        var className = "viewLink";

        if (this.props.active) {
            className += " active";
        }

        return (
            <div style={{display: "inline-block"}}>
                <div className={className}>
                    <button onClick={this.toggleMenu}>Controls</button>
                </div>
                {this.getMenu()}
            </div>
        );
    }
});

var AppHeader = React.createClass({
    mixins: [Reflux.connect(SystemStore, "system")],

    isControlsLocation: function () {
        return _(this.props.location.pathname).startsWith("/main/controls/");
    },

    getServerTime: function () {
        return moment(this.state.system.serverTime).format("DD.MM.YYYY HH:mm:ss");
    },

    render: function () {
        return (
            <div className="appHeader">
                <div>
                    <Link className="viewLink" activeClassName="active" to="/main/overview">
                        <button>Overview</button>
                    </Link>
                    <Link className="viewLink" activeClassName="active" to="/main/stats">
                        <button>Statistics</button>
                    </Link>
                    <ControlsMenu active={this.isControlsLocation()}/>
                </div>
                <div className="time">
                    {this.getServerTime()}
                </div>
            </div>
        );
    }
});

var AppContainer = React.createClass({
    render: function () {
        return (
            <div className="appContainer">
                {this.props.children}
            </div>
        );
    }
});

var App = React.createClass({
    childContextTypes: {
        showNotification: React.PropTypes.func.isRequired
    },

    getChildContext: function () {
        return {
            showNotification: this.showNotification
        };
    },

    showNotification: function (message) {
        this.refs.notificationSystem.addNotification({
            message: message,
            level: "info",
            position: "tc",
            autoDismiss: 2
        });
    },

    getNotificationStyle: function () {
        return {
            NotificationItem: {
                DefaultStyle: {
                    textAlign: "center",
                    fontSize: 15
                },
                info: {
                    borderTop: "4px solid " + Config.mainColor,
                    backgroundColor: "white",
                    color: Config.mainColor
                }
            },
            Dismiss: {
                DefaultStyle: {
                    visibility: "hidden"
                }
            }
        };
    },

    render: function () {
        return (
            <div id="app">
                <AppHeader location={this.props.location}/>
                <AppContainer>
                    {this.props.children}
                </AppContainer>
                <NotificationSystem ref="notificationSystem" style={this.getNotificationStyle()}/>
            </div>
        );
    }
});
module.exports = AuthorizedComponent(App);
