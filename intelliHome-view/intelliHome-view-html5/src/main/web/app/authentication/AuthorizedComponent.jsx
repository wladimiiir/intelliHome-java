var React = require('react');
var Reflux = require('reflux');
var Unlock = require('./UnlockComponent.jsx');
var AuthenticationStore = require('../store/AuthenticationStore');

var AuthorizedComponent = function (Component) {
    return React.createClass({
        mixins: [Reflux.connect(AuthenticationStore, "auth")],

        render: function () {
            if (this.state.auth.authenticated) {
                return <Component {...this.props}/>;
            } else {
                return <Unlock/>;
            }
        }
    });
};
module.exports = AuthorizedComponent;