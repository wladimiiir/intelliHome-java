var React = require('react');
var Config = require('../config/Config');
var AuthenticationActions = require('../actions/AuthenticationActions');
var $ = require('jquery');

var Unlock = React.createClass({
    getInitialState: function () {
        return ({
            password: "",
            wrongPasswordMessageVisible: false
        });
    },

    componentDidMount: function () {
        $(this.refs.password).focus();
    },

    unlock: function (e) {
        e.preventDefault();

        AuthenticationActions.unlock(this.state.password, this.showWrongPasswordMessage);
    },

    showWrongPasswordMessage: function () {
        this.setState({
            wrongPasswordMessageVisible: true
        });
        setTimeout(function () {
            if (this.isMounted()) {
                this.setState({
                    wrongPasswordMessageVisible: false
                });
            }
        }.bind(this), 2000);
    },

    handleChange: function (e) {
        this.setState({
            password: e.target.value
        });
    },

    render: function () {
        return (
            <div className="unlock">
                <form onSubmit={this.unlock}>
                    <input ref="password" type="password" value={this.state.password} onChange={this.handleChange}></input>
                    <button>Unlock</button>
                </form>
                {this.getWrongPasswordElement()}
            </div>
        );
    },

    getWrongPasswordElement: function () {
        return this.state.wrongPasswordMessageVisible
            ? <div className="wrongPassword">Wrong password</div>
            : null;
    }
});

module.exports = Unlock;
