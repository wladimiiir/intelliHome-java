var Reflux = require('reflux');

var UnitHistoryActions = Reflux.createActions([
    "refreshHistory"
]);

module.exports = UnitHistoryActions;