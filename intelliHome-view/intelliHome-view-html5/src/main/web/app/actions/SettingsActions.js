var Reflux = require('reflux');

var SettingsActions = Reflux.createActions([
    "setHistoryVisibility",
    "setHistoryRange"
]);

module.exports = SettingsActions;