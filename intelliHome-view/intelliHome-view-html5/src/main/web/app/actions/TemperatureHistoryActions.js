var Reflux = require('reflux');

var TemperatureHistoryActions = Reflux.createActions([
    "refreshHistory"
]);

module.exports = TemperatureHistoryActions;