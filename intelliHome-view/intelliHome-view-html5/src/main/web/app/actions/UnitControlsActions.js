var Reflux = require('reflux');

var UnitControlsActions = Reflux.createActions([
    "refreshUnitControl",
    "applyUnitControl"
]);

module.exports = UnitControlsActions;