var Reflux = require('reflux');

var AuthenticationActions = Reflux.createActions([
    "unlock"
]);

module.exports = AuthenticationActions;