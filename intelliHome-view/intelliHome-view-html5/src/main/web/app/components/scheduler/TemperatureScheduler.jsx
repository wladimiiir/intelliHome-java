var React = require('react');
var Reflux = require('reflux');
var $ = require('jquery');
var _ = require('lodash');
var Link = require('react-router').Link;
var UnitControlStore = require('../../store/UnitControlStore');
var UnitControlActions = require('../../actions/UnitControlsActions');

jqxBaseFramework = $;
require('jqwidgets-framework/jqwidgets/jqxcore');
require('jqwidgets-framework/jqwidgets/jqxbuttons');
require('jqwidgets-framework/jqwidgets/jqxscrollbar');
require('jqwidgets-framework/jqwidgets/jqxdata');
require('jqwidgets-framework/jqwidgets/jqxdate');
require('jqwidgets-framework/jqwidgets/jqxdatetimeinput');
require('jqwidgets-framework/jqwidgets/jqxcalendar');
require('jqwidgets-framework/jqwidgets/jqxwindow');
require('jqwidgets-framework/jqwidgets/jqxinput');
require('jqwidgets-framework/jqwidgets/jqxcheckbox');
require('jqwidgets-framework/jqwidgets/jqxlistbox');
require('jqwidgets-framework/jqwidgets/jqxdropdownlist');
require('jqwidgets-framework/jqwidgets/jqxnumberinput');
require('jqwidgets-framework/jqwidgets/jqxradiobutton');
require('jqwidgets-framework/jqwidgets/jqxscheduler');
require('jqwidgets-framework/jqwidgets/jqxscheduler.api');

var TemperatureScheduler = React.createClass({
    mixins: [Reflux.connect(UnitControlStore, "controls")],

    contextTypes: {
        showNotification: React.PropTypes.func
    },

    componentDidMount: function () {
        var source = {
            dataType: "array",
            dataFields: [
                {name: "id", type: "string"},
                {name: "startTime", type: "date"},
                {name: "endTime", type: "date"},
                {name: "recurrenceRule", type: "string"},
                {name: "minTemperature", type: "number"},
                {name: "maxTemperature", type: "number"}

            ],
            id: "id",
            localData: []
        };
        this.adapter = new $.jqx.dataAdapter(source);
        var scheduler = $(this.refs.scheduler);

        scheduler.jqxScheduler({
            date: new $.jqx.date("todayDate"),
            width: "100%",
            height: "100%",
            view: "weekView",
            showLegend: true,
            editDialogCreate: this.onEditDialogCreate,
            editDialogOpen: this.onEditDialogOpen,
            renderAppointment: this.renderEvent,
            editDialogDateFormatString: "dd.MM.yyyy",
            editDialogDateTimeFormatString: "dd.MM.yyyy HH:mm",
            localization: {
                firstDay: 1,
                t: "HH:mm"
            },
            resources: {
                colorScheme: "scheme20",
                dataField: "calendar",
                source: new $.jqx.dataAdapter(source)
            },
            appointmentDataFields: {
                id: "id",
                from: "startTime",
                to: "endTime",
                recurrencePattern: "recurrenceRule",
                minTemperature: "minTemperature",
                maxTemperature: "maxTemperature"
            },
            views: [
                {type: "dayView", showWorkTime: false, timeRuler: {formatString: "HH:mm"}},
                {type: "weekView", showWorkTime: false, timeRuler: {formatString: "HH:mm"}},
                {type: "monthView"},
                {type: "agendaView"}
            ]
        });
        scheduler.on("appointmentAdd", function (event) {
            this.updateAppointmentData(event.args.appointment);
        }.bind(this));
        scheduler.on("appointmentChange", function (event) {
            this.updateAppointmentData(event.args.appointment);
        }.bind(this));
        this.updateEvents();
    },

    componentDidUpdate: function (prevProps, prevState) {
        if (!_.isEqual(prevState.controls, this.state.controls)) {
            this.updateEvents();
        }
    },

    onEditDialogCreate: function (dialog, fields, editAppointment) {
        minTempContainer = $(
            '<div>' +
            '<div class="jqx-scheduler-edit-dialog-label">Min (°C)</div>' +
            '<div class="jqx-scheduler-edit-dialog-field"><div id="minTempInput"/></div>' +
            '</div>'
        );
        maxTempContainer = $(
            '<div>' +
            '<div class="jqx-scheduler-edit-dialog-label">Max (°C)</div>' +
            '<div class="jqx-scheduler-edit-dialog-field"><div id="maxTempInput"/></div>' +
            '</div>'
        );

        minTempContainer.find("#minTempInput").jqxNumberInput({width: "100%", inputMode: "simple", spinMode: "simple", spinButtons: true, spinButtonsStep: 0.1});
        maxTempContainer.find("#maxTempInput").jqxNumberInput({width: "100%", inputMode: "simple", spinMode: "simple", spinButtons: true, spinButtonsStep: 0.1});

        fields.subjectContainer.hide();
        fields.locationContainer.hide();
        fields.timeZoneContainer.hide();
        fields.colorContainer.hide();
        fields.statusContainer.hide();

        dialog.children().last().children().first().before(maxTempContainer);
        dialog.children().last().children().first().before(minTempContainer);

        this.dialog = dialog;
    },

    onEditDialogOpen: function (dialog, fields, editAppointment) {
        if (editAppointment) {
            minTempContainer.find("#minTempInput").jqxNumberInput("setDecimal", editAppointment.minTemperature);
            maxTempContainer.find("#maxTempInput").jqxNumberInput("setDecimal", editAppointment.maxTemperature);
        }
    },

    renderEvent: function (data) {
        if(data.appointment == null) {
            return data;
        }
        data.html = "Min:" + data.appointment.minTemperature + "\nMax:" + data.appointment.maxTemperature;
        return data;
    },

    updateAppointmentData: function (appointment) {
        if (this.addedEvent) {
            appointment.minTemperature = this.addedEvent.minTemperature;
            appointment.maxTemperature = this.addedEvent.maxTemperature;
        } else if (this.dialog) {
            appointment.minTemperature = this.dialog.find("#minTempInput").val();
            appointment.maxTemperature = this.dialog.find("#maxTempInput").val();
        }
    },

    getControl: function () {
        var unitControls = this.state.controls[this.props.params.unitID];
        if (!unitControls) {
            return null;
        }

        return _.find(unitControls, function (unitControl) {
            return unitControl.id == this.props.params.unitControlID;
        }.bind(this));
    },

    updateEvents: function () {
        var control = this.getControl();
        if (!control || !control.data.calendarRules) {
            return;
        }

        if (!_.isEqual(control.data.calendarRules, this.calendarRules)) {
            this.calendarRules = _.clone(control.data.calendarRules);
            this.loadSchedulerData(this.calendarRules);
        }
    },

    loadSchedulerData: function (data) {
        var scheduler = $(this.refs.scheduler);
        scheduler.jqxScheduler("beginAppointmentsUpdate");
        scheduler.jqxScheduler("getAppointments").forEach(function (appointment) {
            scheduler.jqxScheduler("deleteAppointment", appointment.id);
        });
        data.forEach(function (event) {
            this.addedEvent = event;
            scheduler.jqxScheduler("addAppointment", {
                id: event.id,
                startTime: new Date(event.startTime),
                endTime: new Date(event.endTime),
                recurrenceRule: event.recurrenceRule,
                minTemperature: event.minTemperature,
                maxTemperature: event.maxTemperature
            });
            this.addedEvent = null;
        }.bind(this));

        scheduler.jqxScheduler("endAppointmentsUpdate");
    },

    getCalendarRules: function () {
        var scheduler = $(this.refs.scheduler);

        return scheduler.jqxScheduler("getAppointments").map(function (appointment) {
            console.log(scheduler.jqxScheduler("getAppointmentProperty", appointment.id, "recurrenceException"));
            var recurrencePattern = scheduler.jqxScheduler("getAppointmentProperty", appointment.id, "recurrencePattern");
            return {
                id: appointment.id,
                startTime: scheduler.jqxScheduler("getAppointmentProperty", appointment.id, "from").toDate(),
                endTime: scheduler.jqxScheduler("getAppointmentProperty", appointment.id, "to").toDate(),
                recurrenceRule: recurrencePattern ? recurrencePattern.toString() : null,
                minTemperature: appointment.minTemperature,
                maxTemperature: appointment.maxTemperature
            };
        });
    },

    showSaveNotification: function () {
        this.context.showNotification("Changes saved.");
    },

    save: function () {
        var control = this.getControl();
        if (!control) {
            return;
        }
        UnitControlActions.applyUnitControl(control, {
            calendarRules: this.getCalendarRules()
        }, this.showSaveNotification);
    },

    render: function () {
        return (
            <div className="temperatureScheduler">
                <div className="scheduler" ref="scheduler">
                </div>
                <div className="buttonPanel">
                    <button onClick={this.save}>Save</button>
                    <Link to={"/main/controls/" + this.props.params.unitID}>
                        <button>Back</button>
                    </Link>
                </div>
            </div>
        );
    }
});
module.exports = TemperatureScheduler;
