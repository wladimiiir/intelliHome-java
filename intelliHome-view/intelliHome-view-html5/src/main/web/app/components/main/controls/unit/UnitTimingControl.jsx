var React = require('react');
var UnitState = require('../../../../constants/UnitState');
var UnitControlsActions = require('../../../../actions/UnitControlsActions');

var UnitTimingControl = React.createClass({
    getInitialState: function () {
        return {
            runTime: 15
        };
    },

    componentDidMount: function () {
        if (this.isRunning()) {
            this.startRefresh();
        }
    },

    componentWillUnmount: function () {
        this.stopRefresh();
    },

    startRefresh: function () {
        this.refreshTimer = setInterval(function () {
            UnitControlsActions.refreshUnitControl(this.props.control);
        }.bind(this), 1000);
    },

    stopRefresh: function () {
        if (this.refreshTimer) {
            clearInterval(this.refreshTimer);
        }
    },

    startClicked: function () {
        UnitControlsActions.applyUnitControl(this.props.control, {
            runTimeMinutes: this.state.runTime
        });
        this.startRefresh();
    },

    cancelClicked: function () {
        UnitControlsActions.applyUnitControl(this.props.control, {
            runTimeMinutes: 0
        });
        this.stopRefresh();
    },

    timingButtonClicked: function (time) {
        this.setState({
            runTime: time
        });
    },

    isRunning: function () {
        return this.props.control.data.remaining > 0;
    },

    createTimingButton: function (runTime, text) {
        return <button className={runTime == this.state.runTime ? "timingButton active" : "timingButton"}
                       onClick={this.timingButtonClicked.bind(this, runTime)}>{text}</button>
    },

    getTimingComponent: function () {
        return (
            <div style={{display: "inline-block"}}>
                {this.createTimingButton(15, "15min")}
                {this.createTimingButton(30, "30min")}
                {this.createTimingButton(60, "1h")}
                {this.createTimingButton(120, "2h")}
            </div>
        );
    },

    getRemainingTime: function () {
        var remainingSeconds = this.props.control.data.remaining;
        var hours = Math.floor(remainingSeconds / (60 * 60));
        var minutes = Math.floor(remainingSeconds / 60) - hours * 60;
        var seconds = remainingSeconds % 60;

        var time = "";
        if (hours > 0) {
            time += hours + " h ";
        }
        if (minutes > 0 || hours > 0) {
            time += minutes + " min ";
        }
        time += seconds + " s";

        return <div className="remainingTime">{time}</div>;
    },

    getActionButton: function () {
        if (this.isRunning()) {
            return <button className="actionButton" onClick={this.cancelClicked}>Cancel</button>;
        } else {
            return <button className="actionButton" onClick={this.startClicked}>Start</button>;
        }
    },

    render: function () {
        return (
            <div className="unitTimingControl">
                {this.isRunning() ? this.getRemainingTime() : this.getTimingComponent()}
                {this.getActionButton()}
            </div>
        );
    }
});
module.exports = UnitTimingControl;