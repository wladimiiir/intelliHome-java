var React = require('react');
var Reflux = require('reflux');
var SettingsActions = require('../../../actions/SettingsActions');
var SettingsStore = require('../../../store/SettingsStore');
var Type = require('../../../constants/HistoryRangeType');
var moment = require('moment');
var $ = require('jquery');
jQuery = $;
require('jquery-datetimepicker');

var Range = React.createClass({
    mixins: [Reflux.connect(SettingsStore, "settings")],

    componentDidMount: function () {
        this.initDateTimePicker();
    },

    componentWillUpdate: function (nextProps, nextState) {
        if (this.state.settings.historyRange.type != nextState.settings.historyRange.type) {
            this.destroyDateTimePicker();
        }
    },

    componentDidUpdate: function (prevProps, prevState) {
        if (this.state.settings.historyRange.type != prevState.settings.historyRange.type) {
            this.initDateTimePicker();
        }
    },

    initDateTimePicker: function () {
        if (!this.refs.dateTimePicker) {
            return;
        }

        $(this.refs.dateTimePicker).datetimepicker({
            format: this.getPickerFormat(),
            timepicker: this.state.settings.historyRange.type == Type.HOUR,
            closeOnDateSelect: this.getCloseOnDateSelect(),
            dayOfWeekStart: 1,
            onChangeDateTime: this.dateTimeChanged
        });
    },

    destroyDateTimePicker: function () {
        if (!this.refs.dateTimePicker) {
            return;
        }
        $(this.refs.dateTimePicker).datetimepicker("destroy");
    },

    getPickerFormat: function () {
        switch (this.state.settings.historyRange.type) {
            case Type.HOUR:
                return "d.m. H:i";
            case Type.DAY:
                return "d.m.Y";
            case Type.MONTH:
                return "MMM Y";
        }
    },

    getCloseOnDateSelect: function () {
        switch (this.state.settings.historyRange.type) {
            case Type.HOUR:
                return false;
            default:
                return true;
        }
    },

    dateTimeChanged: function (date) {
        console.log("Changing to date: " + date);

        var time = moment(this.state.settings.historyRange.date);

        switch (this.state.settings.historyRange.type) {
            case Type.DAY:
                SettingsActions.setHistoryRange(
                    this.getHistoryRange(this.state.settings.historyRange.type, moment(date).hour(time.hour()).minute(time.minute()).toDate())
                );
                break;
            default:
                SettingsActions.setHistoryRange(
                    this.getHistoryRange(this.state.settings.historyRange.type, date)
                );
                break;
        }

    },

    getHistoryRange: function (type, date) {
        return {
            type: type,
            date: date
        };
    },

    typeSelected: function (type) {
        SettingsActions.setHistoryRange(this.getHistoryRange(type, this.state.settings.historyRange.date));
    },

    getTypeButton: function (type, text) {
        return (
            <button className={this.state.settings.historyRange.type == type ? "active" : ""}
                    onClick={this.typeSelected.bind(this, type, this.state.settings.historyRange.date)}>
                {text}
            </button>
        );
    },

    monthChanged: function (e) {
        this.dateTimeChanged(moment(this.state.settings.historyRange.date).month(Number(e.target.value)).date(1).toDate());
    },

    getMonthSelector: function () {
        var monthOptions = [];

        for (var i = 0; i < 12; i++) {
            monthOptions.push(<option key={i} value={i}>{moment().month(i).format("MMMM")}</option>);
        }

        return (
            <select value={moment(this.state.settings.historyRange.date).month()} onChange={this.monthChanged}>
                {monthOptions}
            </select>
        );
    },

    yearChanged: function (e) {
        this.dateTimeChanged(moment(this.state.settings.historyRange.date).year(Number(e.target.value)).month(0).date(1).toDate());
    },

    getYearSelector: function () {
        var yearOptions = [];

        for (var i = new Date().getFullYear(); i >= 2015; i--) {
            yearOptions.push(<option key={i} value={i}>{i}</option>);
        }

        return (
            <select value={this.state.settings.historyRange.date.getFullYear()} onChange={this.yearChanged}>
                {yearOptions}
            </select>
        );
    },

    getRangeDate: function () {
        var time = moment(this.state.settings.historyRange.date);
        switch (this.state.settings.historyRange.type) {
            case Type.HOUR:
                var hour = <input ref="dateTimePicker" type="text" readOnly value={moment(time).format("DD.MM. HH:mm")}></input>;
                return (
                    <div>
                        {hour}
                        <span> - {time.add(1, "h").format("HH:mm")}</span>
                    </div>
                );
            case Type.DAY:
                return <input ref="dateTimePicker" type="text" readOnly value={moment(time).format("DD.MM.YYYY")}></input>;
            case Type.MONTH:
                return (
                    <div>
                        {this.getMonthSelector()}
                        {this.getYearSelector()}
                    </div>
                );
            case Type.YEAR:
                return this.getYearSelector();
            case Type.YEARS:
                return "All time";
        }
    },

    render: function () {
        return (
            <div className="range">
                <div className="typeSelector">
                    {this.getTypeButton(Type.HOUR, "H")}
                    {this.getTypeButton(Type.DAY, "D")}
                    {this.getTypeButton(Type.MONTH, "M")}
                    {this.getTypeButton(Type.YEAR, "Y")}
                    {this.getTypeButton(Type.YEARS, "A")}
                </div>
                <div className="rangeDate">
                    {this.getRangeDate()}
                </div>
            </div>
        );
    }
});

var HistoryRangeComponent = React.createClass({
    mixins: [Reflux.connect(SettingsStore, "settings")],

    previousDate: function () {
        var historyRange = this.state.settings.historyRange;
        var dateTime = moment(historyRange.date);

        switch (historyRange.type) {
            case Type.HOUR:
                dateTime.subtract(1, "h");
                break;
            case Type.DAY:
                dateTime.subtract(1, "d");
                break;
            case Type.MONTH:
                dateTime.subtract(1, "M");
                break;
            case Type.YEAR:
                dateTime.subtract(1, "y");
                break;
        }

        SettingsActions.setHistoryRange({
            type: historyRange.type,
            date: dateTime.toDate()
        });
    },

    nextDate: function () {
        var historyRange = this.state.settings.historyRange;
        var dateTime = moment(historyRange.date);

        switch (historyRange.type) {
            case Type.HOUR:
                dateTime.add(1, "h");
                break;
            case Type.DAY:
                dateTime.add(1, "d");
                break;
            case Type.MONTH:
                dateTime.add(1, "M");
                break;
            case Type.YEAR:
                dateTime.add(1, "y");
                break;
        }

        SettingsActions.setHistoryRange({
            type: historyRange.type,
            date: dateTime.toDate()
        });
    },

    render: function () {
        return (
            <div className="historyRange">
                <div className="previous" onClick={this.previousDate}><i className="fa fa-caret-left"></i></div>
                <Range/>

                <div className="next" onClick={this.nextDate}><i className="fa fa-caret-right"></i></div>
            </div>
        );
    }
});
module.exports = HistoryRangeComponent;