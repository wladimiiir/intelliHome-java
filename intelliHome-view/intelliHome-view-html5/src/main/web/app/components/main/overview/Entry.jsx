var React = require('react');

var Entry = React.createClass({
    render: function () {
        return (
            <div className="entry">
                <div className="name">{this.props.name}</div>
                <div className={"value " + (this.props.valueClass || "")}>{this.props.value}</div>
            </div>
        );
    }
});
module.exports = Entry;