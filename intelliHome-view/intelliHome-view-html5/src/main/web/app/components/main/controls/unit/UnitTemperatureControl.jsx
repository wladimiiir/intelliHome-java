var React = require('react');
var Reflux = require('reflux');
var Link = require('react-router').Link;
var TemperatureStore = require('../../../../store/TemperatureStore');
var UnitControlsActions = require('../../../../actions/UnitControlsActions');
var $ = require('jquery');
var _ = require('lodash');

var UnitTemperatureControl = React.createClass({
    mixins: [
        Reflux.connect(TemperatureStore, "temperatureData")
    ],

    contextTypes: {
        showNotification: React.PropTypes.func
    },

    getInitialState: function () {
        return {
            minTemperature: null,
            maxTemperature: null,
            adjusting: false
        };
    },

    showChangesAppliedMessage: function () {
        this.context.showNotification("Changes applied");
    },

    manualClicked: function () {
        UnitControlsActions.applyUnitControl(this.props.control, {
            min: this.getMinTemperature(),
            max: this.getMaxTemperature(),
            manual: true
        }, this.showChangesAppliedMessage);
        this.setState({
            adjusting: false
        });
    },

    autoClicked: function () {
        UnitControlsActions.applyUnitControl(this.props.control, {
            manual: false
        }, this.showChangesAppliedMessage);
        this.setState({
            adjusting: false
        });
    },

    getActiveClass: function (manual) {
        if (this.props.control.data.manual == manual) {
            return "applyButton active";
        } else {
            return "applyButton";
        }
    },

    getTemperatureName: function () {
        if (!this.state.temperatureData) {
            return null;
        }

        return _.find(this.state.temperatureData, function (temperature) {
            return temperature.id == this.props.control.data.temperatureID;
        }.bind(this)).name;
    },

    minChanged: function (e) {
        this.setState({
            minTemperature: parseFloat(e.target.value),
            maxTemperature: this.getMaxTemperature(),
            adjusting: true
        });
    },

    maxChanged: function (e) {
        this.setState({
            minTemperature: this.getMinTemperature(),
            maxTemperature: parseFloat(e.target.value),
            adjusting: true
        });
    },

    minusClicked: function () {
        this.setState({
            minTemperature: this.getMinTemperature() - 0.5,
            maxTemperature: this.getMaxTemperature() - 0.5,
            adjusting: true
        });
    },

    plusClicked: function () {
        this.setState({
            minTemperature: this.getMinTemperature() + 0.5,
            maxTemperature: this.getMaxTemperature() + 0.5,
            adjusting: true
        });
    },

    getMinTemperature: function () {
        return this.state.adjusting ? this.state.minTemperature : this.props.control.data.min;
    },

    getMaxTemperature: function () {
        return this.state.adjusting ? this.state.maxTemperature : this.props.control.data.max;
    },

    hasCalendarRules: function () {
        return this.props.control.data.calendarRules;
    },

    selectAll: function (e) {
        $(e.target).select();
    },

    render: function () {
        var schedulerLink = null;
        if (this.hasCalendarRules()) {
            schedulerLink =
                <Link to={"/scheduler/temperature/" + this.props.control.unitID + "/" + this.props.control.id}>
                    <button className="schedulerButton"><i className="fa fa-calendar"></i></button>
                </Link>;
        }

        return (
            <div className="unitTemperatureControl">
                <div className="container">
                    <div className="temperatureName">{this.getTemperatureName()}</div>

                    <div className="sectionContainer">
                        <div className="adjustSection">
                            <input type="number" step="0" value={this.getMinTemperature()} onChange={this.minChanged} onFocus={this.selectAll}/>
                            <input type="number" value={this.getMaxTemperature()} onChange={this.maxChanged} onFocus={this.selectAll}/>

                            <button className="adjustButton" onClick={this.minusClicked}><i className="fa fa-angle-down"></i></button>
                            <button className="adjustButton" onClick={this.plusClicked}><i className="fa fa-angle-up"></i></button>
                        </div>

                        <div className="applySection">
                            <button className={this.getActiveClass(true)}
                                    onClick={this.manualClicked}>
                                Manual
                            </button>
                            <button className={this.getActiveClass(false)}
                                    onClick={this.autoClicked}>
                                Auto
                            </button>
                            {schedulerLink}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});
module.exports = UnitTemperatureControl;