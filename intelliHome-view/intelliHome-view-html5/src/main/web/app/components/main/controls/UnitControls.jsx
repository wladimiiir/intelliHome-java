var React = require('react');
var Reflux = require('reflux');
var _ = require('lodash');
var Loading = require('../../../common/Loading.jsx');
var UnitControlStore = require('../../../store/UnitControlStore');
var UnitStore = require('../../../store/UnitStore');
var UnitControlManager = require('./unit/UnitControlManager.jsx');

var UnitControls = React.createClass({
    mixins: [Reflux.connect(UnitControlStore, "controls"), Reflux.connect(UnitStore, "unitData")],

    getUnitControls: function () {
        if (this.state.controls && this.state.controls[this.props.unitID]) {
            return this.state.controls[this.props.unitID];
        }

        return null;
    },

    toUnitControl: function (unitControl) {
        return UnitControlManager.createUnitControlComponent(unitControl);
    },

    getUnit: function () {
        if (!this.state.unitData) {
            return null;
        }

        return _(this.state.unitData).find(function (unitData) {
            return unitData.id == this.props.unitID;
        }.bind(this));
    },

    getUnitName: function () {
        var unit = this.getUnit();
        return unit == null ? null : unit.name;
    },

    getUnitState: function () {
        var unit = this.getUnit();
        return unit == null ? null : unit.state;
    },

    render: function () {
        var unitControls = this.getUnitControls();

        if (!unitControls) {
            return <Loading/>;
        }

        var unitState = this.getUnitState();
        return (
            <div className="unitControls">
                <div className="unitName">{this.getUnitName()}</div>
                <div className={"unitState " + unitState}>{unitState}</div>
                {unitControls.map(this.toUnitControl)}
            </div>
        );
    }
});
module.exports = UnitControls;