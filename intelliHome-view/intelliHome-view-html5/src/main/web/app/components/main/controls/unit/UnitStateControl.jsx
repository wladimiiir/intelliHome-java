var React = require('react');
var Link = require('react-router').Link;
var UnitState = require('../../../../constants/UnitState');
var UnitControlsActions = require('../../../../actions/UnitControlsActions');

var UnitStateControl = React.createClass({
    onClicked: function () {
        UnitControlsActions.applyUnitControl(this.props.control, {
            action: UnitState.ON
        });
    },

    offClicked: function () {
        UnitControlsActions.applyUnitControl(this.props.control, {
            action: UnitState.OFF
        });
    },

    autoClicked: function () {
        UnitControlsActions.applyUnitControl(this.props.control, {
            action: UnitState.AUTO
        });
    },

    getActiveClass: function (activeState, otherActiveState) {
        if (this.props.control.data.state == activeState || this.props.control.data.state == otherActiveState) {
            return "active";
        } else {
            return "";
        }
    },

    hasCalendarRules: function () {
        return this.props.control.data.calendarRules;
    },

    render: function () {
        var schedulerLink = null;
        if (this.hasCalendarRules()) {
            schedulerLink =
                <Link to={"/scheduler/state/" + this.props.control.unitID + "/" + this.props.control.id}>
                    <button className="schedulerButton"><i className="fa fa-calendar"></i></button>
                </Link>;
        }

        return (
            <div className="unitStateControl">
                <button className={this.getActiveClass(UnitState.FORCE_RUN)}
                        onClick={this.onClicked}>
                    On
                </button>
                <button className={this.getActiveClass(UnitState.SUSPENDED)}
                        onClick={this.offClicked}>
                    Off
                </button>
                <button className={this.getActiveClass(UnitState.STARTED, UnitState.STOPPED)}
                        onClick={this.autoClicked}>
                    Auto
                </button>
                {schedulerLink}
            </div>
        );
    }
});
module.exports = UnitStateControl;