var React = require('react');
var UnitStateControl = require('./UnitStateControl.jsx');
var UnitTemperatureControl = require('./UnitTemperatureControl.jsx');
var UnitTimingControl = require('./UnitTimingControl.jsx');

var Manager = {
    createUnitControlComponent: function (unitControl) {
        switch (unitControl.type) {
            case "STATE":
                return <UnitStateControl key={unitControl.id} control={unitControl}/>;
            case "TEMPERATURE":
                return <UnitTemperatureControl key={unitControl.id} control={unitControl}/>;
            case "TIMING":
                return <UnitTimingControl key={unitControl.id} control={unitControl}/>;
            default:
                return null;
        }
    }
};

module.exports = Manager;