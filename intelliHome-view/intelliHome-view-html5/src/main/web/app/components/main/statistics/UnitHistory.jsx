var React = require('react');
var _ = require('lodash');
var Reflux = require('reflux');
var Loading = require('../../../common/Loading.jsx');
var Type = require('../../../constants/HistoryRangeType');
var Chart = require('react-google-charts').Chart;
var UnitHistoryStore = require('../../../store/UnitHistoryStore');
var SettingsStore = require('../../../store/SettingsStore');
var SettingsActions = require('../../../actions/SettingsActions');

var UnitHistory = React.createClass({
    mixins: [
        Reflux.connect(UnitHistoryStore, "history"),
        Reflux.connect(SettingsStore, "settings")
    ],

    getInitialState: function () {
        return {
            unitHistory: null,
            graphComponent: null
        };
    },

    componentDidMount: function () {
        this.setState({
            unitHistory: this.state.history[this.props.id],
            graphComponent: this.getGraphComponent(this.state.history[this.props.id])
        });
    },

    componentDidUpdate: function () {
        var unitHistory = this.state.history[this.props.id];

        if (!_.isEqual(this.state.unitHistory, unitHistory)) {
            this.setState({
                unitHistory: unitHistory,
                graphComponent: this.getGraphComponent(unitHistory)
            });
        }
    },

    getHeight: function () {
        switch (this.state.settings.historyRange.type) {
            case Type.HOUR:
            case Type.DAY:
                return 120;
            case Type.MONTH:
            case Type.YEAR:
            case Type.YEARS:
                return 90;
            default:
                return 120;
        }
    },

    getFormat: function () {
        switch (this.state.settings.historyRange.type) {
            case Type.HOUR:
            case Type.DAY:
                return "HH:mm";
            case Type.MONTH:
                return "d";
            case Type.YEAR:
                return "MMM";
            case Type.YEARS:
                return "y";
            default:
                return "HH:mm";
        }
    },

    getGraphOptions: function () {
        return ({
            chartArea: {
                left: 40,
                width: "100%"
            },
            height: this.getHeight(),
            timeline: {
                groupByRowLabel: true,
                rowLabelStyle: {
                    fontName: "Helvetica",
                    fontSize: 16
                },
                barLabelStyle: {
                    fontSize: 16
                }
            },
            tooltip: {
                trigger: "selection"
            },
            hAxis: {
                format: this.getFormat()
            },
            legend: {position: "none"},
            bar: {groupWidth: "90%"}
        });
    },

    getChartType: function () {
        switch (this.state.settings.historyRange.type) {
            case Type.HOUR:
            case Type.DAY:
                return "Timeline";
            case Type.MONTH:
            case Type.YEAR:
            case Type.YEARS:
                return "ColumnChart";
            default:
                return "Timeline";
        }
    },

    getGraphComponent: function (history) {
        if (!history) {
            return null;
        }

        return (
            <Chart chartType={this.getChartType()}
                   graph_id={"unitHistory_" + this.props.id}
                   rows={history.data.map(this.toRow)}
                   columns={this.getColumns()}
                   options={this.getGraphOptions()}
                />
        );
    },

    getColumns: function () {
        switch (this.state.settings.historyRange.type) {
            case Type.HOUR:
            case Type.DAY:
                return [
                    {type: "string"},
                    {type: "string"},
                    {type: "datetime"},
                    {type: "datetime"}
                ];
            case Type.MONTH:
            case Type.YEAR:
            case Type.YEARS:
                return [
                    {type: "datetime"},
                    {type: "number"}
                ];
            default:
                return [];
        }
    },

    toRow: function (history) {
        switch (this.state.settings.historyRange.type) {
            case Type.HOUR:
            case Type.DAY:
                return [
                    "State",
                    history.state,
                    new Date(history.startTime),
                    new Date(history.endTime)
                ];
            case Type.MONTH:
            case Type.YEAR:
            case Type.YEARS:
                return [
                    new Date(history.time),
                    Math.round(history.runningTime)
                ];
            default:
                return [];
        }
    },

    isVisible: function () {
        return this.state.settings.historyVisibility[this.props.id];
    },

    toggleVisibility: function () {
        SettingsActions.setHistoryVisibility(this.props.id, !this.isVisible());
    },

    getContent: function () {
        if (this.state.graphComponent == null) {
            return null;
        }

        return (
            <div>
                {this.state.graphComponent}
            </div>
        );
    },

    render: function () {
        return (
            <div ref="component" className={"unitHistory" + (this.isVisible() ? "" : " hidden")}>
                <div className="header" onClick={this.toggleVisibility}>
                    <div className="name">{this.props.name}</div>
                    <div className="showHideButton">
                        <i className={"fa " + (this.isVisible() ? "fa-chevron-up" : "fa-chevron-down")}></i>
                    </div>
                </div>
                {this.getContent() || <Loading size={20}/>}
            </div>
        );
    }
});
module.exports = UnitHistory;