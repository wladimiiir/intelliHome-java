var React = require('react');
var Reflux = require('reflux');
var HistoryRangeComponent = require('./statistics/HistoryRangeComponent.jsx');
var TemperatureHistory = require('./statistics/TemperatureHistory.jsx');
var UnitHistory = require('./statistics/UnitHistory.jsx');
var TemperatureStore = require('../../store/TemperatureStore');
var UnitStore = require('../../store/UnitStore');

var Statistics = React.createClass({
    mixins: [
        Reflux.connect(TemperatureStore, "temperatureData"),
        Reflux.connect(UnitStore, "unitData")
    ],

    componentDidMount: function () {

    },

    render: function () {
        return (
            <div className="statistics">
                <HistoryRangeComponent/>
                {(this.state.temperatureData || []).map(this.toTemperatureHistory)}
                {(this.state.unitData || []).map(this.toUnitHistory)}
            </div>
        );
    },

    toTemperatureHistory: function (temperature) {
        return <TemperatureHistory key={temperature.id} id={temperature.id} name={temperature.name}/>;
    },

    toUnitHistory: function (unit) {
        return <UnitHistory key={unit.id} id={unit.id} name={unit.name}/>;
    }
});
module.exports = Statistics;