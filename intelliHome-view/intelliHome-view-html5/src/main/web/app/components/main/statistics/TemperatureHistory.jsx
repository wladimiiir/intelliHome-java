var React = require('react');
var _ = require('lodash');
var Reflux = require('reflux');
var Loading = require('../../../common/Loading.jsx');
var Chart = require('react-google-charts').Chart;
var TemperatureHistoryStore = require('../../../store/TemperatureHistoryStore');
var TemperatureHistoryActions = require('../../../actions/TemperatureHistoryActions');
var SettingsStore = require('../../../store/SettingsStore');
var SettingsActions = require('../../../actions/SettingsActions');

var TemperatureHistory = React.createClass({
    mixins: [
        Reflux.connect(TemperatureHistoryStore, "history"),
        Reflux.connect(SettingsStore, "settings")
    ],

    getInitialState: function () {
        return {
            temperatureHistory: null,
            graphComponent: null
        };
    },

    componentDidMount: function () {
        this.setState({
            temperatureHistory: this.state.history[this.props.id],
            graphComponent: this.getGraphComponent(this.state.history[this.props.id])
        });
    },

    componentDidUpdate: function () {
        var temperatureHistory = this.state.history[this.props.id];

        if (!_.isEqual(this.state.temperatureHistory, temperatureHistory)) {
            this.setState({
                temperatureHistory: temperatureHistory,
                graphComponent: this.getGraphComponent(temperatureHistory)
            });
        }
    },

    getGraphOptions: function () {
        return ({
            vAxis: {
                title: "°C"
            },
            legend: {
                position: "none"
            },
            chartArea: {
                left: 50,
                top: 5,
                width: this.refs.component.clientWidth - 60,
                height: 145
            },
            backgroundColor: "transparent",
            explorer: {
                axis: "horizontal",
                keepInBounds: true
            },
            series: [
                {color: "green"},
                {color: "blue"},
                {color: "red"}
            ],
            tooltip: {
                trigger: "selection"
            }
        });
    },

    getGraphComponent: function (history) {
        if (!history) {
            return null;
        }

        return (
            <Chart chartType="LineChart"
                   graph_id={"temperature_" + this.props.id}
                   rows={history.data.map(this.toRow)}
                   columns={this.getColumns()}
                   options={this.getGraphOptions()}
                   width="100%"
                   height="100%"
                />
        );
    },

    getColumns: function () {
        return [
            {type: "datetime"},
            {type: "number"},
            {type: "number"},
            {type: "number"}
        ];
    },

    toRow: function (history) {
        return [
            new Date(history.time),
            history.temperature,
            history.minTemperature,
            history.maxTemperature
        ]
    },

    isVisible: function () {
        return this.state.settings.historyVisibility[this.props.id];
    },

    toggleVisibility: function () {
        var visible = !this.isVisible();
        SettingsActions.setHistoryVisibility(this.props.id, visible);
    },

    getContent: function () {
        if (this.state.graphComponent == null) {
            return null;
        }

        return (
            <div>
                {this.state.graphComponent}
            </div>
        );
    },

    render: function () {
        return (
            <div ref="component" className={"temperatureHistory" + (this.isVisible() ? "" : " hidden")}>
                <div className="header" onClick={this.toggleVisibility}>
                    <div className="name">{this.props.name}</div>
                    <div className="showHideButton">
                        <i className={"fa " + (this.isVisible() ? "fa-chevron-up" : "fa-chevron-down")}></i>
                    </div>
                </div>
                {this.getContent() || <Loading size={20}/>}
            </div>
        );
    }
});
module.exports = TemperatureHistory;