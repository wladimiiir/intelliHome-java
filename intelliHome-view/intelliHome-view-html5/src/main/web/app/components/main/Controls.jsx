var React = require('react');
var UnitControls = require('./controls/UnitControls.jsx');

var Controls = React.createClass({
    render: function () {
        return (
            <div className="controls">
                <UnitControls unitID={this.props.params.unitID}/>
            </div>
        );
    }
});
module.exports = Controls;