var React = require('react');
var Temperatures = require('./overview/Temperatures.jsx');
var Units = require('./overview/Units.jsx');

var Overview = React.createClass({
    render: function () {
        return (
            <div className="overview">
                <Temperatures/>
                <Units/>
            </div>
        );
    }
});
module.exports = Overview;