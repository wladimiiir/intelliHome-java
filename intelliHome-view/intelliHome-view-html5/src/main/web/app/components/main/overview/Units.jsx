var React = require('react');
var Reflux = require('reflux');
var Loading = require('../../../common/Loading.jsx');
var Entry = require('./Entry.jsx');
var UnitStore = require('../../../store/UnitStore');

var Units = React.createClass({
    mixins: [Reflux.connect(UnitStore, "units")],

    getEntries: function () {
        if (!this.state.units) {
            return <Loading/>;
        }

        return this.state.units.map(function (unit, index) {
            return <Entry key={index} name={unit.name} value={unit.state.toUpperCase()} valueClass={this.getValueClass(unit.state)}/>
        }.bind(this));
    },

    getValueClass: function (state) {
        return state.charAt(0).toLowerCase() + state.slice(1);
    },

    render: function () {
        return (
            <div className="units">
                <div className="header">Units</div>
                <div className="entries">
                    {this.getEntries()}
                </div>
            </div>
        );
    }
});
module.exports = Units;