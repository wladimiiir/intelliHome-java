var React = require('react');
var Reflux = require('reflux');
var Loading = require('../../../common/Loading.jsx');
var Entry = require('./Entry.jsx');
var TemperatureStore = require('../../../store/TemperatureStore');

var Temperatures = React.createClass({
    mixins: [Reflux.connect(TemperatureStore, "data")],

    getEntries: function () {
        if (!this.state.data) {
            return <Loading/>;
        }

        return this.state.data.map(function (temperature) {
            return <Entry key={temperature.id} name={temperature.name} value={this.getValue(temperature)}/>
        }.bind(this));
    },

    getValue: function (temperature) {
        return temperature.value.toFixed(2) +" °C";
    },

    render: function () {
        return (
            <div className="temperatures">
                <div className="header">Temperatures</div>
                <div className="entries">
                    {this.getEntries()}
                </div>
            </div>
        );
    }
});
module.exports = Temperatures;