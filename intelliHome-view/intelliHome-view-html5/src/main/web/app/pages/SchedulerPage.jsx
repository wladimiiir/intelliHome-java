var React = require('react');

var SchedulerPage = React.createClass({
    render: function () {
        return (
            <div className="schedulerPage">
                {this.props.children}
            </div>
        );
    }
});
module.exports = SchedulerPage;