var React = require('react');

var MainPage = React.createClass({
    render: function () {
        return (
            <div className="mainPage">
                {this.props.children}
            </div>
        );
    }
});
module.exports = MainPage;