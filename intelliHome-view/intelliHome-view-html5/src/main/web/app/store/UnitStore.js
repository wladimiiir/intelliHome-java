var Reflux = require('reflux');
var Config = require('../config/Config');
var $ = require('jquery');

var UnitStore = Reflux.createStore({
    data: null,

    init: function () {
        this.refreshUnitData();
    },

    getInitialState: function () {
        return this.data;
    },

    refreshUnitData: function () {
        $.ajax({
            url: Config.resourceHost + "/unit",
            method: "GET",
            dataType: "json",
            cache: false,
            success: function (data) {
                this.data = data;
                this.trigger(data);
            }.bind(this),
            complete: function () {
                setTimeout(this.refreshUnitData, 1000);
            }.bind(this)
        });
    }
});

module.exports = UnitStore;
