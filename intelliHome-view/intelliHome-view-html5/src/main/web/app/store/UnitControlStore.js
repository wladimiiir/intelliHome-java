var Reflux = require('reflux');
var Config = require('../config/Config');
var Actions = require('../actions/UnitControlsActions');
var UnitStore = require('./UnitStore');
var $ = require('jquery');
var _ = require('lodash');

var UnitControlStore = Reflux.createStore({
    listenables: [Actions],
    controls: null,

    init: function () {
        this.listenTo(UnitStore, this.onUnitStoreChange);
    },

    getInitialState: function () {
        return this.controls;
    },

    onUnitStoreChange: function (data) {
        if (this.controls == null) {
            this.loadControls(data);
        }
    },

    loadControls: function (data) {
        this.controls = {};
        data.map(function (unit) {
                return unit.id;
            })
            .forEach(this.loadUnitControls);
    },

    loadUnitControls: function (unitID) {
        $.ajax({
            url: Config.resourceHost + "/unit/" + unitID + "/controls",
            type: 'GET',
            headers: {
                'Accept': ['application/json'],
                'Content-Type': 'application/json'
            },
            cache: false,
            success: function (data) {
                this.controls[unitID] = data;
                this.trigger(_.clone(this.controls));
            }.bind(this)
        });
    },

    refreshUnitControl: function (unitControl) {
        $.ajax({
            url: Config.resourceHost + "/unit/" + unitControl.unitID + "/controls/" + unitControl.id,
            type: 'GET',
            headers: {
                'Accept': ['application/json'],
                'Content-Type': 'application/json'
            },
            cache: false,
            success: function (data) {
                var unitControls = this.controls[unitControl.unitID];
                unitControls[_.findIndex(unitControls, function (control) {
                    return unitControl.id == control.id;
                })] = data;
                this.trigger(_.clone(this.controls));
            }.bind(this)
        });
    },

    applyUnitControl: function (unitControl, controlData, onSuccess) {
        $.ajax({
            url: Config.resourceHost + "/unit/" + unitControl.unitID + "/controls/" + unitControl.id,
            type: 'POST',
            headers: {
                'Accept': ['application/json'],
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(controlData),
            cache: false,
            complete: function () {
                this.refreshUnitControl(unitControl);
            }.bind(this),
            success: onSuccess
        });
    }
});

module.exports = UnitControlStore;
