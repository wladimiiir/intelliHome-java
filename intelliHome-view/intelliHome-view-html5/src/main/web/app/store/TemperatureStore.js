var Reflux = require('reflux');
var Config = require('../config/Config');
var $ = require('jquery');

var TemperatureStore = Reflux.createStore({
    data: null,

    init: function () {
        this.refreshTemperatureData();
    },

    getInitialState: function () {
        return this.data;
    },

    refreshTemperatureData: function () {
        $.ajax({
            url: Config.resourceHost + "/temperature",
            method: "GET",
            dataType: "json",
            cache: false,
            success: function (data) {
                this.data = data;
                this.trigger(this.data);
            }.bind(this),
            complete: function () {
                setTimeout(this.refreshTemperatureData, 1000);
            }.bind(this)
        });
    }
});

module.exports = TemperatureStore;
