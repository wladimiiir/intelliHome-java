var Reflux = require('reflux');
var $ = require('jquery');
var _ = require('lodash');
var Actions = require('../actions/SettingsActions');
var HistoryRangeType = require('../constants/HistoryRangeType');
var moment = require('moment');

var SettingsStore = Reflux.createStore({
    listenables: [Actions],
    settings: {
        historyVisibility: {},
        historyRange: {
            type: HistoryRangeType.HOUR,
            date: moment().minute(0).second(0).millisecond(0).toDate()
        }
    },

    init: function () {
    },

    getInitialState: function () {
        return _.clone(this.settings, true);
    },

    setHistoryVisibility: function (historyID, visible) {
        this.settings.historyVisibility[historyID] = visible;
        this.trigger(_.clone(this.settings, true));
    },

    setHistoryRange: function (range) {
        this.settings.historyRange = _.clone(range, true);
        this.trigger(_.clone(this.settings, true));
    }
});

module.exports = SettingsStore;
