var Reflux = require('reflux');
var Config = require('../config/Config');
var UnitStore = require('./UnitStore');
var SettingsStore = require('./SettingsStore');
var $ = require('jquery');
var _ = require('lodash');

var UnitHistoryStore = Reflux.createStore({
    data: [],
    currentSettings: SettingsStore.getInitialState(),
    history: {},
    historyRequests: {},

    init: function () {
        this.listenTo(UnitStore, this.onUnitStoreChange);
        this.listenTo(SettingsStore, this.onSettingsChange)
    },

    getInitialState: function () {
        return this.history;
    },

    onUnitStoreChange: function (data) {
        this.data = data;
    },

    onSettingsChange: function (nextSettings) {
        var previousSettings = this.currentSettings;
        this.currentSettings = nextSettings;

        if (!_.isEqual(previousSettings.historyRange, nextSettings.historyRange)) {
            this.refreshHistory();
            return;
        }
        if (!_.isEqual(previousSettings.historyVisibility, nextSettings.historyVisibility)) {
            this.data.forEach(function (unit) {
                if (!previousSettings.historyVisibility[unit.id] && nextSettings.historyVisibility[unit.id]) {
                    this.refreshUnitHistory(unit);
                }
            }.bind(this));
        }
    },

    refreshHistory: function () {
        this.data.forEach(this.refreshUnitHistory);
    },

    refreshUnitHistory: function (unit) {
        if (this.historyRequests[unit.id] != undefined) {
            this.historyRequests[unit.id].abort();
        }

        this.history[unit.id] = null;
        this.trigger(this.history);

        if (!this.currentSettings.historyVisibility[unit.id]) {
            return;
        }

        this.historyRequests[unit.id] = $.ajax({
            url: Config.resourceHost + "/unit/" + unit.id + "/history",
            type: 'POST',
            headers: {
                'Accept': ['application/json'],
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(this.currentSettings.historyRange),
            cache: false,
            success: function (data) {
                this.history[unit.id] = data;
                this.historyRequests[unit.id] = undefined;
                this.trigger(this.history);
            }.bind(this)
        });
    }
});

module.exports = UnitHistoryStore;
