var Reflux = require('reflux');
var Config = require('../config/Config');
var $ = require('jquery');
var _ = require('lodash');
var moment = require('moment');

var SystemStore = Reflux.createStore({
    data: {
        localTime: new Date(),
        serverTime: new Date()
    },

    init: function () {
        this.loadServerTime();
    },

    getInitialState: function () {
        return _.clone(this.data, true);
    },

    startDateTimeTimer: function () {
        setInterval(function () {
            var currentTime = new Date();
            var diff = currentTime.getTime() - this.data.localTime.getTime();

            this.data.localTime = currentTime;
            this.data.serverTime = new Date(this.data.serverTime.getTime() + diff);
            this.trigger(this.data);
        }.bind(this), 1000);
    },

    loadServerTime: function () {
        $.ajax({
            url: Config.resourceHost + "/system/datetime",
            method: "GET",
            dataType: "json",
            cache: false,
            success: function (data) {
                this.data.localTime = new Date();
                this.data.serverTime = new Date(data.datetime);
                this.trigger(this.data);
                this.startDateTimeTimer();
            }.bind(this)
        });
    }
});

module.exports = SystemStore;
