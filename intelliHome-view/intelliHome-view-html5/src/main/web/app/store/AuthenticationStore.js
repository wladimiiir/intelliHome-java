var Reflux = require('reflux');
var Config = require('../config/Config');
var $ = require('jquery');
var Actions = require('../actions/AuthenticationActions');

var AuthenticationStore = Reflux.createStore({
    listenables: [Actions],
    authenticated: false,

    init: function () {
        this.checkAuthentication();
    },

    getInitialState: function () {
        return this.getState();
    },

    getState: function () {
        return {
            authenticated: this.authenticated
        };
    },

    checkAuthentication: function () {
        $.ajax({
            url: Config.resourceHost + "/auth",
            method: "GET",
            dataType: "json",
            cache: false,
            success: function (data) {
                this.authenticated = data.authenticated;
                this.trigger(this.getState());
            }.bind(this)
        });
    },

    unlock: function (password, onError) {
        $.ajax({
            url: Config.resourceHost + "/auth",
            method: "POST",
            headers: {
                'Accept': ['application/json'],
                'Content-Type': 'application/json'
            },
            cache: false,
            data: JSON.stringify({
                password: password
            }),
            success: function (data) {
                this.authenticated = data.authenticated;
                this.trigger(this.getState());
            }.bind(this),
            error: onError
        });
    }
});

module.exports = AuthenticationStore;
