var Reflux = require('reflux');
var Config = require('../config/Config');
var TemperatureStore = require('./TemperatureStore');
var SettingsStore = require('./SettingsStore');
var $ = require('jquery');
var _ = require('lodash');

var TemperatureHistoryStore = Reflux.createStore({
    data: [],
    currentSettings: SettingsStore.getInitialState(),
    history: {},
    historyRequests: {},

    init: function () {
        this.listenTo(TemperatureStore, this.onTemperatureStoreChange);
        this.listenTo(SettingsStore, this.onSettingsChange)
    },

    getInitialState: function () {
        return this.history;
    },

    onTemperatureStoreChange: function (data) {
        this.data = data;
    },

    onSettingsChange: function (nextSettings) {
        var previousSettings = this.currentSettings;
        this.currentSettings = nextSettings;

        if (!_.isEqual(previousSettings.historyRange, nextSettings.historyRange)) {
            this.refreshHistory();
            return;
        }
        if (!_.isEqual(previousSettings.historyVisibility, nextSettings.historyVisibility)) {
            this.data.forEach(function (temperature) {
                if (!previousSettings.historyVisibility[temperature.id] && nextSettings.historyVisibility[temperature.id]) {
                    this.refreshTemperatureHistory(temperature);
                }
            }.bind(this));
        }
    },

    refreshHistory: function () {
        this.data.forEach(this.refreshTemperatureHistory);
    },

    refreshTemperatureHistory: function (temperature) {
        if (this.historyRequests[temperature.id] != undefined) {
            this.historyRequests[temperature.id].abort();
        }

        this.history[temperature.id] = null;
        this.trigger(this.history);

        if (!this.currentSettings.historyVisibility[temperature.id]) {
            return;
        }

        this.historyRequests[temperature.id] = $.ajax({
            url: Config.resourceHost + "/temperature/" + temperature.id + "/history",
            type: 'POST',
            headers: {
                'Accept': ['application/json'],
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(this.currentSettings.historyRange),
            cache: false,
            success: function (data) {
                this.history[temperature.id] = data;
                this.historyRequests[temperature.id] = undefined;
                this.trigger(this.history);
            }.bind(this)
        });
    }
});

module.exports = TemperatureHistoryStore;
