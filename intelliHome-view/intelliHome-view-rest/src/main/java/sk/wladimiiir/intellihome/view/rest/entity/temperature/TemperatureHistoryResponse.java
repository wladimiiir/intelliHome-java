package sk.wladimiiir.intellihome.view.rest.entity.temperature;

import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public class TemperatureHistoryResponse {
	private List<TemperatureHistoryEntity> data;

	public TemperatureHistoryResponse(List<TemperatureHistoryEntity> data) {
		this.data = data;
	}

	public List<TemperatureHistoryEntity> getData() {
		return data;
	}

	public void setData(List<TemperatureHistoryEntity> data) {
		this.data = data;
	}
}
