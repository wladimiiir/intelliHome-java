package sk.wladimiiir.intellihome.view.rest;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.Date;

/**
 * System related REST resource.
 */
@Named
@Singleton
@Path("/system")
public class SystemResource {

    /**
     * Constructor.
     */
    public SystemResource() {
    }

    /**
     * @return Test that system is up.
     */
    @GET
    @Path("/ping")
    public Response ping() {
        return Response.ok().build();
    }

    @GET
    @Path("/datetime")
    public Response getDate() {
        return Response.ok(Collections.singletonMap("datetime", new Date())).build();
    }

}
