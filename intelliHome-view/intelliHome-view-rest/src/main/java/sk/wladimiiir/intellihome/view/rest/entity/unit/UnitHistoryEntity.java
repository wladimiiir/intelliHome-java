package sk.wladimiiir.intellihome.view.rest.entity.unit;

import java.util.Date;

/**
 * @author Vladimir Hrusovsky
 */
public class UnitHistoryEntity {
    private Date time;
	private Date startTime;
    private Date endTime;
    private String state;
	private long runningTime;

    public UnitHistoryEntity() {
    }

    public UnitHistoryEntity(Date startTime, Date endTime, String state) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.state = state;
	}

	public UnitHistoryEntity(Date time, long runningTime) {
		this.time = time;
		this.runningTime = runningTime;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

	public long getRunningTime() {
		return runningTime;
	}

	public void setRunningTime(long runningTime) {
		this.runningTime = runningTime;
	}
}
