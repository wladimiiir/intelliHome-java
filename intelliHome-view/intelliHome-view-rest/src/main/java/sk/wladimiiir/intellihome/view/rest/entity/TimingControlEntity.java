package sk.wladimiiir.intellihome.view.rest.entity;

/**
 * @author Vladimir Hrusovsky
 */
public class TimingControlEntity {
    private Long runTimeSeconds;
    private Long remainingSeconds;

    public TimingControlEntity() {
    }

    public TimingControlEntity(long remainingSeconds) {
        this.remainingSeconds = remainingSeconds;
    }

    public Long getRunTimeSeconds() {
        return runTimeSeconds;
    }

    public void setRunTimeSeconds(Long runTimeSeconds) {
        this.runTimeSeconds = runTimeSeconds;
    }

    public Long getRemainingSeconds() {
        return remainingSeconds;
    }

    public void setRemainingSeconds(Long remainingSeconds) {
        this.remainingSeconds = remainingSeconds;
    }
}
