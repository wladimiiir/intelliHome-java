package sk.wladimiiir.intellihome.view.rest.entity.unit;

import java.util.List;

/**
 * @author Vladimir Hrusovsky
 */
public class UnitHistoryResponse {
	private List<UnitHistoryEntity> data;

	public UnitHistoryResponse(List<UnitHistoryEntity> data) {
		this.data = data;
	}

	public List<UnitHistoryEntity> getData() {
		return data;
	}

	public void setData(List<UnitHistoryEntity> data) {
		this.data = data;
	}
}
