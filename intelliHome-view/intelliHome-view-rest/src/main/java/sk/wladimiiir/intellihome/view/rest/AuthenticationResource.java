package sk.wladimiiir.intellihome.view.rest;

import sk.wladimiiir.intellihome.service.auth.AuthenticationService;
import sk.wladimiiir.intellihome.service.auth.NoAuthorization;
import sk.wladimiiir.intellihome.view.rest.entity.auth.AuthenticationRequest;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;

/**
 * @author Vladimir Hrusovsky
 */
@Named
@Singleton
@Path("auth")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AuthenticationResource {
    @Inject
    private volatile AuthenticationService authenticationService;

    @GET
    @NoAuthorization
    public Response isAuthenticated(@Context HttpServletRequest request) {
        return Response.ok(Collections.singletonMap("authenticated", authenticationService.isAuthenticated(request))).build();
    }

    @POST
    @NoAuthorization
    public Response authenticate(AuthenticationRequest authRequest) {
        return Response.ok(Collections.singletonMap("authenticated", authenticationService.authenticate(authRequest.getPassword()))).build();
    }
}
