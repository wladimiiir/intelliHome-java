package sk.wladimiiir.intellihome.view.rest.entity;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author Vladimir Hrusovsky
 */
public class TemperatureControlEntity {
	private String name;
    private float min;
    private float max;
    private boolean manual;

    public TemperatureControlEntity() {
    }

    public TemperatureControlEntity(String name, float min, float max, boolean manual) {
        this.name = name;
		this.min = min;
        this.max = max;
        this.manual = manual;
    }

	@XmlElement
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement
    public float getMin() {
        return min;
    }

    public void setMin(float min) {
        this.min = min;
    }

    @XmlElement
    public float getMax() {
        return max;
    }

    public void setMax(float max) {
        this.max = max;
    }

    @XmlElement
    public boolean isManual() {
        return manual;
    }

    public void setManual(boolean manual) {
        this.manual = manual;
    }
}
