package sk.wladimiiir.intellihome.view.rest.entity.temperature;

import java.util.Date;

/**
 * @author Vladimir Hrusovsky
 */
public class TemperatureHistoryEntity {
    private Date time;
    private Float temperature;
	private Float minTemperature;
	private Float maxTemperature;

    public TemperatureHistoryEntity() {
    }

    public TemperatureHistoryEntity(Date time, Float temperature) {
        this.time = time;
        this.temperature = temperature;
    }

	public TemperatureHistoryEntity(Date time, Float temperature, Float minTemperature, Float maxTemperature) {
		this.time = time;
		this.temperature = temperature;
		this.minTemperature = minTemperature;
		this.maxTemperature = maxTemperature;
	}

	public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Float getTemperature() {
        return temperature;
    }

    public void setTemperature(Float temperature) {
        this.temperature = temperature;
    }

	public Float getMinTemperature() {
		return minTemperature;
	}

	public void setMinTemperature(Float minTemperature) {
		this.minTemperature = minTemperature;
	}

	public Float getMaxTemperature() {
		return maxTemperature;
	}

	public void setMaxTemperature(Float maxTemperature) {
		this.maxTemperature = maxTemperature;
	}
}
