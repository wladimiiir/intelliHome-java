package sk.wladimiiir.intellihome.view.rest.entity.unit;

/**
 * @author Vladimir Hrusovsky
 */
public class UnitEntity {
    private String id;
    private String name;
    private String state;
    private String stateID;

	public UnitEntity() {
	}

	public UnitEntity(String id, String name, String state, String stateID) {
		this.id = id;
		this.name = name;
		this.state = state;
		this.stateID = stateID;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateID() {
        return stateID;
    }

    public void setStateID(String stateID) {
        this.stateID = stateID;
    }
}
