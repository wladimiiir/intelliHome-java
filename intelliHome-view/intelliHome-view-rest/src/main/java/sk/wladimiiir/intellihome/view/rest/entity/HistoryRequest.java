package sk.wladimiiir.intellihome.view.rest.entity;

import java.util.Date;

/**
 * @author Vladimir Hrusovsky
 */
public class HistoryRequest {
	public enum Type {
		HOUR,
		DAY,
		MONTH,
		YEAR,
		YEARS
	}

	private Type type;
	private Date date;

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
