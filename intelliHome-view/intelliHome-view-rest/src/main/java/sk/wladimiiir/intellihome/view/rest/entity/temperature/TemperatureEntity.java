package sk.wladimiiir.intellihome.view.rest.entity.temperature;

/**
 * @author Vladimir Hrusovsky
 */
public class TemperatureEntity {
	private String id;
	private String name;
	private Float value;

	public TemperatureEntity() {
	}

	public TemperatureEntity(String id, String name, Float value) {
		this.id = id;
		this.name = name;
		this.value = value;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}
}
