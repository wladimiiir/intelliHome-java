package sk.wladimiiir.intellihome.view.rest.entity.unit.control;

import java.util.Map;

/**
 * @author Vladimir Hrusovsky
 */
public class UnitControl {
    private String id;
    private String type;
    private String unitID;
    private Map<String, Object> data;

    public UnitControl() {
    }

    public UnitControl(String id, String type, String unitID, Map<String, Object> data) {
        this.id = id;
        this.type = type;
        this.unitID = unitID;
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUnitID() {
        return unitID;
    }

    public void setUnitID(String unitID) {
        this.unitID = unitID;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
