package sk.wladimiiir.intellihome.view.rest.entity.auth;

/**
 * @author Vladimir Hrusovsky
 */
public class AuthenticationRequest {
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
